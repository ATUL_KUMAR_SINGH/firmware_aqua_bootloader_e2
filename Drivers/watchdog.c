#include "stm32f0xx_iwdg.h"
#include "stm32f0xx_rcc.h"
#include "watchdog.h"
#include "pin_config.h"

#include "std_periph_headers.h"

void init_watchdog (void) {
	/* Check if the system has resumed from IWDG reset */
	if (RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET) {
		/* Clear reset flags */
		RCC_ClearFlag();
	}

	/* IWDG timeout equal to 250 ms (the timeout may varies due to LSI frequency dispersion) */
	/* Enable write access to IWDG_PR and IWDG_RLR registers */
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	
	/* IWDG counter clock: LSI/32 */
	IWDG_SetPrescaler(IWDG_Prescaler_32);
	
	/* Set counter reload value to obtain 250ms IWDG TimeOut.
	Counter Reload Value	= 250ms/IWDG counter clock period
							= 250ms / (LSI/32)
							= 0.25s / (LsiFreq/32)
							= LsiFreq/(32 * 4)
							= LsiFreq/128
	*/
	IWDG_SetReload(128);
	
	/* Reload IWDG counter */
	IWDG_ReloadCounter();
	
	/* Enable IWDG (the LSI oscillator will be enabled by hardware) */
	IWDG_Enable();

    /* Reload IWDG counter */
    IWDG_ReloadCounter();  
}
