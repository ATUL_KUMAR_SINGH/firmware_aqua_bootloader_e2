/************************************************************************//**
* @file			json_client.c
*
* @brief		Contains  the API for packet validation from GUI
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 5/2/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
//#include "bitops.h"
#include <stm32f0xx.h>
#include <RTL.h>
#include <Net_Config.h>
#include <string.h>
#include <stdio.h>
#include "tcp.h"
#include "c_func.h"
#include "Target.h"
#include "buzzer.h"
#include "client_registration.h"
#include "stm32f0xx_rtc.h"
#include "automated_tasks.h"
//#include "AT45DB161D.h"
//#include "spi.h"
#include "rtc.h"
//#include "logs.h"
#include "schedule.h"
#include "calibration.h"
#include "sensor.h"
//#include "virtual_sensor.h"
#include"json_client.h"
#include "memory_map.h"
#include"timer.h"
#include "water_management.h"
#include "uart.h"
#include "eeprom.h"
#include "buyers_n_client_reg.h"


 /*
**===========================================================================
**		structures declaration section
**===========================================================================
*/
extern serial_port  json_client;		//cloud_server			//atul
extern struct json_struct	json_info;
//struct gui_payload json_info;				 
uint8_t json_send_arr[1000], *json_send_ptr; 		//atul
uint8_t *json_send_ptr = &json_send_arr[0];	  //atul
extern uint8_t update_nw_settings, glbl_arr[10];
extern  uint8_t  oht_pump_trigger_cause, ugt_pump_trigger_cause;
uint32_t send_cloud_server_status;
extern OS_TID tid_pump_evt;
extern struct wms_sys_info wms_sys_config;
extern struct buzzer_switch_behavior buzzer_switch_info;
//extern struct tank_info OHT_tank, UGT_tank;
 uint32_t current_hour_oht_consumption[24], current_hour_ugt_consumption[24];
extern RTC_TimeTypeDef RTC_TimeStructure;
extern RTC_InitTypeDef RTC_InitStructure;
extern RTC_DateTypeDef date_obj;
extern struct sys_info system_info;
extern uint8_t JSON_packet_status, total_reg_client, firmware_upgrade_flag, buzzer_set_level;
volatile extern uint32_t driver_time_out;
//extern LOCALM ip_config;
extern struct client_info	http_user_info[TOTAL_HTTP_SESSION]; 
extern U8 *error_code;
uint32_t pump_on_count, pump_off_count;
extern uint32_t udp_broadcast_counter, oht_notification_info, ugt_notification_info, system_notification_info, event_info;
uint8_t udp_broadcast_flag, udp_broadcast_state;
extern struct real_sensor_info  oht_real_sensor_config[TOTAL_OHT_TANK], ugt_real_sensor_config;
extern struct tank_setting oht_tank_config[TOTAL_OHT_TANK], ugt_tank_config;	//atul
extern uint16_t auto_task_num_flag;
//extern uint8_t auto_sub_task_num_flag;
extern uint8_t Neotech_Header[], OHT_triggered;  // UGT_triggered;  //atul
uint8_t pump_on_off_flag;
extern struct auto_task_state auto_task_pump_state[9];
//extern  LOCALM localm[];
extern unsigned char license_type, license_class, total_client_license;

extern uint8_t HwVersion[3];


#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif


extern struct tank_immediate_setting oht_immed , ugt_immed;   	//atul
extern RTC_TimeTypeDef last_pump_triggered_time_OHT;  // last_pump_triggered_time_UGT;  //atul
extern RTC_DateTypeDef last_pump_triggered_date_OHT;  // last_pump_triggered_date_UGT;
extern uint8_t OHT_pump_state; // UGT_pump_state;  //atul

extern uint8_t gui_send_data[2];
extern uint32_t sensor_malfunctioning_notification_field;

/*
**===========================================================================
**		Global Variable Declaration Section
**		
**===========================================================================
*/

uint8_t JSON_send_arr[200];
char *g_send_ptr = "{\"sid\":\"9000\",\"sad\":\"xx\",\"cmd\":\"    \",\"data\":\" \"}";
char *g_log_ptr = "{\"sid\":\"9000\",\"sad\":\"x\",\"cmd\":\"    \",\"total\":";


/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/
/************************************************************************//**
*	uint8_t __inline validate_start(uint8_t *ptr)
*
* @brief			This routine check the start byte.
*				
*
* @param 			*ptr - pointer to buffer.
*
* @returns			1 - if found
*					0 - otherwise
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				01/12/14
* @note				None.                                            
****************************************************************************/
uint8_t __inline validate_start(uint8_t *ptr){
	
	if(*ptr == '<')
		return 1;
	else 
		return 0;			

}

/************************************************************************//**
*		void response_tank_pump_settings(uint8_t tank_num){
*
* @brief		This routine creates the response of tank pump settings.
*
* @param		tank_num - tank number	
*				
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/14
*
* @note			
****************************************************************************/
void response_tank_pump_settings(uint8_t tank_num){

	uint8_t indx = 0,  total_tank;
	uint8_t tank_pump_setting_configured = 0;
	uint8_t field_name_indx = 1, len;
	struct real_sensor_info  *sensor_config_ptr; 
	struct tank_setting *tank_config_ptr; 
	struct tank_info *tank_info_ptr; 
	
	//eeprom_data_read_write(TANK_PUMP_SENSOR_SETTING_ADDR, WRITE_OP,&tank_pump_setting_configured, 1); // line only for test
	eeprom_data_read_write(TANK_PUMP_SENSOR_SETTING_ADDR, READ_OP, &tank_pump_setting_configured, 1);
	if(tank_pump_setting_configured == 1) {
	ADD_START_BRACE;

	if(tank_num >= 1 && tank_num <= 25){				// oht
		sensor_config_ptr 	=	(struct real_sensor_info *)&oht_real_sensor_config[indx];
		tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
		tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
		total_tank 			= 	wms_sys_config.total_oht;
	}
//	else if(tank_num >= 26 && tank_num <= 50){				// ugt				//atul
//		sensor_config_ptr 	=	(struct real_sensor_info *)&ugt_real_sensor_config;
//		tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
//		tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
//		total_tank 			= 	wms_sys_config.total_ugt;
//	}
	while(indx < total_tank){
		if(tank_num >= 1 && tank_num <= 25){				// oht
			sensor_config_ptr 	=	(struct real_sensor_info *)&oht_real_sensor_config[indx];
			tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
			tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
			total_tank 			= 	wms_sys_config.total_oht;
		}
//		else if(tank_num >= 26 && tank_num <= 50){				// ugt		//atul
//			sensor_config_ptr 	=	(struct real_sensor_info *)&ugt_real_sensor_config;
//			tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
//			tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
//			total_tank 			= 	wms_sys_config.total_ugt;
//		}
		
		// add tank info 		
		add_field_name(field_name_indx++);
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(tank_config_ptr->tank_num, json_send_ptr, 2);		/* add tank number */
		json_send_ptr += 2;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		
		add_field_name(field_name_indx++);
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(tank_config_ptr->tank_state, json_send_ptr++, 1);			/* add tank state */
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;	
		
		add_field_name(field_name_indx++);											/* add tank name */
		len = strlen((char *)&tank_config_ptr->tank_name[0]);										
		ADD_DOUBLE_QUOTE;
		memcpy(json_send_ptr, &tank_config_ptr->tank_name[0], len);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		
		add_field_name(field_name_indx++);										  /* add tank volume */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(tank_config_ptr->tank_volume, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;


	  // extract pump info
	   	add_field_name(field_name_indx++);										  /* add tank volume */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(tank_info_ptr->pump_select, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		
		add_field_name(field_name_indx++);										  /* add tank volume */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(tank_info_ptr->flow_rate, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;

	  // extract sensor info
		add_field_name(field_name_indx++);										  /* add starting sensor*/
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(sensor_config_ptr->port_num[0], json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);										  /* add end sensor*/
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(sensor_config_ptr->port_num[sensor_config_ptr->total_sensor - 1], json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);										  /* add sensor positioning flag */
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(sensor_config_ptr->equidistant_flag, json_send_ptr++, 1);
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		indx++;
	}
	json_send_ptr--;
	ADD_CLOSING_BRACE;
 }
	else {
	*(json_send_ptr++) = '"';
	 dec_ascii_byte(1, json_send_ptr, 1);	
		json_send_ptr += 1;
	 *json_send_ptr++ = '"';
	}
}


/************************************************************************//**
*		   uint32_t pump_trigger_save_status(uint8_t tank_num){
*
* @brief		This function update the counter for pump dry run.
*
* param         		none
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/

void get_buzzer_settings(){
	uint8_t field_name_indx = 1, len;
	
	if(buzzer_switch_info.buzzer_activate_flag == 1){
		ADD_START_BRACE;
		add_field_name(field_name_indx++);
	}
	ADD_DOUBLE_QUOTE;
	dec_ascii_byte(buzzer_switch_info.buzzer_activate_flag, json_send_ptr++, 1);		/* add tank state */
	ADD_DOUBLE_QUOTE;
	if(buzzer_switch_info.buzzer_activate_flag == 1){
		ADD_COMMA;
		add_field_name(field_name_indx++);										  /* add tank flow rate*/
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(buzzer_switch_info.buzzer_snooze_dur/ 60, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		
		ADD_COMMA;
		add_field_name(field_name_indx++);										  /* add tank flow rate*/
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(buzzer_set_level, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
	}
	if(buzzer_switch_info.buzzer_activate_flag == 1){
		ADD_CLOSING_BRACE;
	}
	if(buzzer_switch_info.buzzer_activate_flag == 2){
		buzzer_switch_info.buzzer_activate_flag = 1;
	}

}


		

	
/************************************************************************//**
*		  void get_pump_status(struct tank_info *tank_ptr){{
*
* @brief		This function create the packet containg currrent pump status.
*
* param         		none
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				06/11/14
* @note			
****************************************************************************/
void get_pump_status(struct tank_info *tank_ptr){
	uint8_t field_name_indx = 1, len, loop, current_val_oht_notification;
	

	if(OHT_triggered) {
	oht_immed.pump_state = OHT_pump_state;
	oht_immed.year       = last_pump_triggered_date_OHT.RTC_Year;
	oht_immed.month		 = last_pump_triggered_date_OHT.RTC_Month;
	oht_immed.date		 = last_pump_triggered_date_OHT.RTC_Date;
	oht_immed.hour		 = last_pump_triggered_time_OHT.RTC_Hours;
	oht_immed.min		 = last_pump_triggered_time_OHT.RTC_Minutes;
	}

//	if(UGT_triggered) {
//	ugt_immed.pump_state = UGT_pump_state;
//	ugt_immed.year       = last_pump_triggered_date_UGT.RTC_Year;
//	ugt_immed.month		 = last_pump_triggered_date_UGT.RTC_Month;
//	ugt_immed.date		 = last_pump_triggered_date_UGT.RTC_Date;
//	ugt_immed.hour		 = last_pump_triggered_time_UGT.RTC_Hours;
//	ugt_immed.min		 = last_pump_triggered_time_UGT.RTC_Minutes;
//	}

//	status.oht_pump_ptr->last_on_off_time[0]	= oht_immed.year;
//	status.oht_pump_ptr->last_on_off_time[1]	= oht_immed.month;
//	status.oht_pump_ptr->last_on_off_time[2]	= oht_immed.date;
//	status.oht_pump_ptr->last_on_off_time[3]	= oht_immed.hour;
//	status.oht_pump_ptr->last_on_off_time[4]	= oht_immed.min;
//	status.oht_pump_ptr->last_on_off_time[5]	= 0;

	memcpy((uint8_t *)&status.oht_pump_ptr->last_on_off_time, (uint8_t *)&oht_immed.year, 5);
	memcpy((uint8_t *)&status.ugt_pump_ptr->last_on_off_time, (uint8_t *)&ugt_immed.year, 5);

	/* Write immediate setting */
	eeprom_data_read_write(IMMEDIATE_DATA, WRITE_OP, (uint8_t *)&oht_immed, 7);
	eeprom_data_read_write(IMMEDIATE_DATA + 7, WRITE_OP, (uint8_t *)&ugt_immed, 7);

	ADD_START_BRACE;

	add_field_name(field_name_indx++);				/* add tank number */
	ADD_DOUBLE_QUOTE;
	len = dec_ascii_arr(tank_ptr->tank_config_ptr->tank_num, json_send_ptr);
	json_send_ptr += len; 
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add pump state */
	ADD_DOUBLE_QUOTE;
	if(tank_ptr->tank_config_ptr->tank_num >= 1 && tank_ptr->tank_config_ptr->tank_num <= 25)
		dec_ascii_byte(status.oht_pump_ptr->pump_state, json_send_ptr++, 1);		
	else
		dec_ascii_byte(status.ugt_pump_ptr->pump_state, json_send_ptr++, 1);					
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add DATE */
	ADD_DOUBLE_QUOTE;
	for(loop = 0; loop < 6 ; loop++){
		if(tank_ptr->tank_config_ptr->tank_num >= 1 && tank_ptr->tank_config_ptr->tank_num <= 25)
			dec_ascii_byte(status.oht_pump_ptr->last_on_off_time[loop], json_send_ptr, 2);
		else
			dec_ascii_byte(status.ugt_pump_ptr->last_on_off_time[loop], json_send_ptr, 2);			
		json_send_ptr += 2; 	
		*(json_send_ptr++) = '-' ;	
		
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;
	
	add_field_name(field_name_indx++);				/* add OHT notification  oht_notification_info*/
	ADD_DOUBLE_QUOTE;
	for(loop = 0; loop < 32 ; loop++){
		if( ((oht_pump_trigger_cause >> loop) & (0x01) ) == 1) {
			current_val_oht_notification = loop;
			break;
		}
	}
	len = dec_ascii_arr(current_val_oht_notification, json_send_ptr);
	json_send_ptr += len; 
	ADD_DOUBLE_QUOTE;
	ADD_CLOSING_BRACE;
		
}
/************************************************************************//**
*		  void get_pump_status(struct tank_info *tank_ptr){{
*
* @brief		This function create the packet containg currrent tank and pump status.
*
* param         		none
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				05/12/14
* @note			
****************************************************************************/

void get_tank_pump_status(void){
	uint8_t field_name_indx = 1, len, loop;

	if(OHT_triggered) {
	oht_immed.pump_state = OHT_pump_state;
	oht_immed.year       = last_pump_triggered_date_OHT.RTC_Year;
	oht_immed.month		 = last_pump_triggered_date_OHT.RTC_Month;
	oht_immed.date		 = last_pump_triggered_date_OHT.RTC_Date;
	oht_immed.hour		 = last_pump_triggered_time_OHT.RTC_Hours;
	oht_immed.min		 = last_pump_triggered_time_OHT.RTC_Minutes;
	}

//	if(UGT_triggered) {
//	ugt_immed.pump_state = UGT_pump_state;
//	ugt_immed.year       = last_pump_triggered_date_UGT.RTC_Year;
//	ugt_immed.month		 = last_pump_triggered_date_UGT.RTC_Month;
//	ugt_immed.date		 = last_pump_triggered_date_UGT.RTC_Date;
//	ugt_immed.hour		 = last_pump_triggered_time_UGT.RTC_Hours;
//	ugt_immed.min		 = last_pump_triggered_time_UGT.RTC_Minutes;
//	}

		/* read buzzer setting */
//	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR, READ_OP, (uint8_t *)&buzzer_switch_info.buzzer_activate_flag, 1);			/* buzzer activation flag */			
//	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 1, READ_OP, (uint8_t *)&buzzer_switch_info.buzzer_snooze_dur, 1);			/* buzzer snooze duration */
//	buzzer_switch_info.buzzer_snooze_dur *= 60;
//	eeprom_data_read_write(BUZZER_SETTING_INFO_ADDR + 2, READ_OP, (uint8_t *)&buzzer_set_level, 1);
//
//	/* read immediate setting */
//	eeprom_data_read_write(IMMEDIATE_DATA, READ_OP, (uint8_t *)&oht_immed.pump_state, 7);
//	status.oht_pump_ptr->pump_state = oht_immed.pump_state;
	//for(temp = 0; temp < 5; temp++){
//		status.oht_pump_ptr->last_on_off_time[0]	= oht_immed.year;
//		status.oht_pump_ptr->last_on_off_time[1]	= oht_immed.month;
//		status.oht_pump_ptr->last_on_off_time[2]	= oht_immed.date;
//		status.oht_pump_ptr->last_on_off_time[3]	= oht_immed.hour;
//		status.oht_pump_ptr->last_on_off_time[4]	= oht_immed.min;
//		status.oht_pump_ptr->last_on_off_time[5]	= 0;
	//}
	memcpy((uint8_t *)&status.oht_pump_ptr->last_on_off_time, (uint8_t *)&oht_immed.year, 5);
	memcpy((uint8_t *)&status.ugt_pump_ptr->last_on_off_time, (uint8_t *)&ugt_immed.year, 5);

	/* Write immediate setting */
	eeprom_data_read_write(IMMEDIATE_DATA, WRITE_OP, (uint8_t *)&oht_immed, 7);
	eeprom_data_read_write(IMMEDIATE_DATA + 7, WRITE_OP, (uint8_t *)&ugt_immed, 7);


//	if(wms_sys_config.total_ugt > 0){
//		eeprom_data_read_write(IMMEDIATE_DATA + 7, READ_OP, (uint8_t *)&ugt_immed.pump_state, 7);
//		memcpy((uint8_t *)&status.ugt_pump_ptr->last_on_off_time, (uint8_t *)&ugt_immed.year, 5);
//	}



	ADD_START_BRACE

	add_field_name(field_name_indx++);				/* add tank number */
	ADD_DOUBLE_QUOTE;
	len = dec_ascii_arr(OHT_tank[0].tank_config_ptr->tank_num, json_send_ptr);
	json_send_ptr += len; 
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add currnet level*/
	ADD_DOUBLE_QUOTE;
	dec_ascii_byte(status.oht_current_level, json_send_ptr, 3);		
	json_send_ptr += 3;
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add virtual currnet level*/
	ADD_DOUBLE_QUOTE;
	dec_ascii_byte(0, json_send_ptr, 3);		
	json_send_ptr += 3;
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add pump state */
	ADD_DOUBLE_QUOTE;
	dec_ascii_byte(status.oht_pump_ptr->pump_state, json_send_ptr++, 1);		
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add DATE */
	ADD_DOUBLE_QUOTE;
	for(loop = 0; loop < 6 ; loop++){
		dec_ascii_byte(status.oht_pump_ptr->last_on_off_time[loop], json_send_ptr, 2);
		json_send_ptr += 2; 	
		*(json_send_ptr++) = '-' ;	
		
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;
	if(wms_sys_config.total_ugt > 0){
		ADD_COMMA;
		add_field_name(field_name_indx++);				/* add tank number */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(UGT_tank.tank_config_ptr->tank_num, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(UGT_tank.current_level, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add virtual currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(UGT_tank.virtual_current_level, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;

		add_field_name(field_name_indx++);				/* add pump state */
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(status.ugt_pump_ptr->pump_state, json_send_ptr++, 1);		
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add DATE */
		ADD_DOUBLE_QUOTE;
		for(loop = 0; loop < 6 ; loop++){
			dec_ascii_byte(status.ugt_pump_ptr->last_on_off_time[loop],json_send_ptr, 2);
			json_send_ptr += 2; 	
			*(json_send_ptr++) = '-' ;	
			
		}
		json_send_ptr--;
		ADD_DOUBLE_QUOTE;
	}			

	ADD_CLOSING_BRACE;
}


/************************************************************************//**
*		  void get_tank_status(struct tank_info *tank_ptr){{
*
* @brief		This function create the packet containg currrent tank status.
*
* param         		none
*
*Return                none 
*
* @author			Nikhil Kukreja
* @date				10/12/15
* @note			
****************************************************************************/

void create_get_tank_level_packet(void){
	uint8_t field_name_indx = 1, len, indx = 0;
	struct tank_setting *tank_config_ptr; 
	struct tank_info *tank_info_ptr; 

	ADD_START_BRACE
	while(indx < wms_sys_config.total_oht){
		tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
		tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
	
		add_field_name(field_name_indx++);				/* add tank number */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(tank_config_ptr->tank_num, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(tank_info_ptr->current_level, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add virtual currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(0, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		indx++;
	}
	indx = 0;
	while(indx < wms_sys_config.total_ugt){
		tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
		tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
		
		add_field_name(field_name_indx++);				/* add tank number */
		ADD_DOUBLE_QUOTE;
		len = dec_ascii_arr(tank_config_ptr->tank_num, json_send_ptr);
		json_send_ptr += len; 
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(tank_info_ptr->current_level, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
	
		add_field_name(field_name_indx++);				/* add virtual currnet level*/
		ADD_DOUBLE_QUOTE;
		dec_ascii_byte(0, json_send_ptr, 3);		
		json_send_ptr += 3;
		ADD_DOUBLE_QUOTE;
		ADD_COMMA;
		indx++;
	}
	json_send_ptr--;
	ADD_CLOSING_BRACE;	

}


/************************************************************************//**
*		  void create_notification_packet(struct tank_info *tank_ptr, uint8_t *info){
*
* @brief		This function create the packet containg currrent tank and pump status.
*
* param         struct tank_info *tank_ptr -  pointer to the structure containing tank info
*			   						*info 	- pointer to the buffer containing notification info
*Return                none 
*
* @author			Nikhil Kukreja
* @date				05/12/14
* @note			
****************************************************************************/
void create_notification_packet(struct tank_info *tank_ptr, uint8_t *info){
	uint8_t	field_name_indx = 1, len, loop;
	uint32_t sensor_malfunctioning_info; 

	ADD_START_BRACE;

	add_field_name(field_name_indx++);				/* add tank number */
	ADD_DOUBLE_QUOTE;
	len = dec_ascii_arr(tank_ptr->tank_config_ptr->tank_num, json_send_ptr);
	json_send_ptr += len; 
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);				/* add notification number */
	ADD_DOUBLE_QUOTE;
	len = dec_ascii_arr(*info, json_send_ptr);
	json_send_ptr += len; 		
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	if((*info) == 1){									/* sensor malfunctioning */
		info++;
		add_field_name(field_name_indx++);				/* add DATE */
		ADD_DOUBLE_QUOTE;	
		sensor_malfunctioning_info = tank_ptr->real_sensor_ptr->malfunctioning_sensor_bits;
		for(loop = 0; loop < 32 ; loop++){
			if(sensor_malfunctioning_info & 0x01 == 1){
				len = dec_ascii_arr(loop + 1, json_send_ptr);
				json_send_ptr += len; 
				*(json_send_ptr++) = '-' ;		
			}
			sensor_malfunctioning_info >>= 1;
		}
		json_send_ptr--;
		ADD_DOUBLE_QUOTE;
	}
	info++;
	add_field_name(field_name_indx++);				/* add DATE */
	ADD_DOUBLE_QUOTE;
	for(loop = 0; loop < 6 ; loop++){
		len = dec_ascii_arr(*info++, json_send_ptr);
		json_send_ptr += len; 	
		*(json_send_ptr++) = '-' ;	
		
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;

	ADD_CLOSING_BRACE;
}

/************************************************************************//**
*		  void create_notification_packet(struct tank_info *tank_ptr, uint8_t *info){
*
* @brief		This function create the packet containg currrent tank and pump status.
*
* param         struct tank_info *tank_ptr -  pointer to the structure containing tank info
*			   						*info 	- pointer to the buffer containing notification info
*Return                none 
*
* @author			Nikhil Kukreja
* @date				05/12/14
* @note			
*************************************************************************************/
uint8_t send_tank_pump_status_GUI(uint32_t tank1_status, uint32_t tank2_status, uint8_t *send_arr){
	uint8_t ret = 0, notification_info[20];
	uint32_t count, len;

	json_send_ptr = &json_send_arr[0];
	if(tank1_status > 0){										 
		if(tank1_status & 0x01){								 		/* create packet for OHT pump status */
			create_GUI_tx_packet(GET_PUMP_STATUS,&OHT_tank[0], 0);
			len = strlen((char *)json_send_arr);
			strncpy((char *)send_arr, (char *)json_send_arr, len);
			send_arr +=  len;
			ret = 1;
		}
		tank1_status >>= 1;
		for(count = 1; count <= TOTAL_NOTIFICATION, tank1_status > 0; count++){		   /* create packet for OHT notification */
			if(tank1_status & 0x01){
				//save_notification(&OHT_tank[0], count, &notification_info[0]);
				create_GUI_tx_packet(SEND_NOTIFICATION, &OHT_tank[0], &notification_info[1]);
				len = strlen((char *)json_send_arr);
				strncpy((char *)send_arr, (char *)json_send_arr,len);
				send_arr +=  len;
				ret = 1;
  			}
			tank1_status >>= 1;	
		}
	}
	if(tank2_status > 0){
		if(tank2_status & 0x01){										/* create packet for UGT pump status */
			create_GUI_tx_packet(GET_PUMP_STATUS,&UGT_tank, 0);
			len = strlen((char *)json_send_arr);
			strncpy((char *)send_arr, (char *)json_send_arr, len);
			send_arr +=  len;
			ret = 1;
		}
		tank2_status >>= 1;
		for(count = 1; count <= TOTAL_NOTIFICATION, tank2_status > 0; count++){			   /* create packet for UGT notification */
			if(tank2_status & 0x01){
				notification_info[0] = count;
			//	save_notification(&UGT_tank, count, &notification_info[0]);
				create_GUI_tx_packet(SEND_NOTIFICATION, &UGT_tank, &notification_info[1]);
				len = strlen((char *)json_send_arr);
				strncpy((char *)send_arr, (char *)json_send_arr,len);
				send_arr +=  len;
				ret = 1;
  			}
			tank2_status >>= 1;	
		}
	}
	*(send_arr++) = '\0';
	
	return ret;
}


/***************************************************************************************//**
*		void create_GUI_tx_packet(uint32_t command, struct tank_info *tank_ptr, uint8_t *param){
*
* @brief			This routine creates the packet for GUI and cloud server.
*				
*
* @param 			command 					- command number
*					struct tank_info *tank_ptr 	- pointer to structure contains tank information. 
*	 				*param 						- *pointer to buffer containing data
*
* @returns			None
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
void create_GUI_tx_packet(uint32_t command, struct tank_info *tank_ptr, uint8_t *param){
	uint8_t arr[4], field_name_indx = 1, len, count, temp, indx;

	intit_JSON_send_header();
	dec_ascii_byte(command, arr, 4);
	strncpy((char *)json_send_ptr, (const char *)arr, 4);		/* Add command field */
	json_send_ptr += 13;										/* Jump pointer to payload part */

	switch(command){
		
		case GUI_WMS_AUTHENTICATE_MAC_ID:
			if(*param == 1)					/* if value -1 then create packet 	*/
		   		add_field_val((uint8_t *)&system_info.mac_id[0], 12);	
			else{							/* otherwise authenticate the packet */
				if((*param == 0x31) || (*param == 0x32) || (*param == 0x33))
					add_field_val(param, 1);
				else if(*param == 0x30){
					ADD_START_BRACE;
					add_field_name(field_name_indx++);
					ADD_DOUBLE_QUOTE;
					*json_send_ptr++ = *param;				
					ADD_DOUBLE_QUOTE;
					ADD_COMMA;

					add_field_name(field_name_indx++);				
					ADD_DOUBLE_QUOTE;
					#ifdef FW_VER_GREATER_THAN_3_7_25
						*json_send_ptr++ = *param;	
					#else
						strncpy((char *)json_send_ptr, (char *)&glbl_arr[1], REGISTRATION_KEY_LEN);
					//	strncpy((char *)json_send_ptr, (char *)&http_user_info[total_reg_client].passwrd_info[0], REGISTRATION_KEY_LEN);
						json_send_ptr += REGISTRATION_KEY_LEN; 
					#endif
				//	strncpy((char *)json_send_ptr, (char *)&glbl_arr[1], REGISTRATION_KEY_LEN);
				//	json_send_ptr += REGISTRATION_KEY_LEN; 
					ADD_DOUBLE_QUOTE;
					ADD_CLOSING_BRACE;		
				}
			}
		break;

		case GUI_WMS_DEVICE_REGISTRATION:
				
				ADD_DOUBLE_QUOTE;
				*json_send_ptr++ = gui_send_data[0];				
				ADD_DOUBLE_QUOTE;
				
		break;
			
			
	 case BUYERS_REGISTRATION:

				ADD_START_BRACE;
				add_field_name(field_name_indx++);
				ADD_DOUBLE_QUOTE;
				*json_send_ptr++ = gui_send_data[0];				
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;

				add_field_name(field_name_indx++);				
				ADD_DOUBLE_QUOTE;
				*json_send_ptr++ = gui_send_data[1];;	

				ADD_DOUBLE_QUOTE;
				ADD_CLOSING_BRACE;	
	
		break;
			
			
		case WMS_GUI_AC_MAINS_STATUS:
			add_field_val(param, 1);
	  	break;

		case GUI_WMS_VIEW_DEVICE_REGISTRATION_LIST:
					fetch_device_registration_list();
		break;

		case GUI_WMS_UPDATE_DEVICE_REGISTRATION_LIST:
		//	fetch_device_registration_list();
		break;
		
		case GUI_WMS_DEVICE_REGISTRATION_REMOVE:
			add_field_val(param, 1);
		break;

		case GUI_WMS_SAVE_DATE_TIME:
			add_field_val(param, 1);
		break;

		case GUI_WMS_GET_DATE_TIME:
			read_GUI_time_create_packet();
		break;

		case SET_NW_SETTING:
			add_field_val(param, 1);
		break;

		case GET_NW_SETTING:
			get_nw_settings();
		break;

		case GUI_WMS_DEVICE_PASSWORD_AUTHENTICATE:
			add_field_val(param, 1);
		break;
		
		case GUI_WMS_DEVICE_PASSWORD_CHANGE:
			add_field_val(param, 1);
		break;

		case GUI_WMS_DEVICE_PASSWORD_FORGET:
	   		add_field_val(param, 1);
		break;

		case GUI_WMS_SET_BUZZER_STATE:
		case GUI_WMS_GET_BUZZER_STATE:
			get_buzzer_settings();
		break;

		case SAVE_WATER_USAGE_SETTING:
			param++;
			if(*param == 1){
		   		param++;
//				ADD_START_BRACE;
//				add_field_name(field_name_indx++);	

			}
			else if(*param == 2){
				param++;
			}
		break;

		case GET_WATER_USAGE_SETTING:
			 get_automated_task_status(param);	
		break;

		case SET_TANK_PUMP_SENSOR_SETTINGS:
		     response_tank_pump_settings(tank_ptr->tank_config_ptr->tank_num);	   //pending
		 break;
		case GET_TANK_PUMP_SENSOR_SETTINGS:
			response_tank_pump_settings(OHT_tank[0].tank_config_ptr->tank_num);
//			if(wms_sys_config.total_ugt > 0){	
//				ADD_CLOSING_BRACE;
//				memcpy(json_send_ptr, json_send_arr, 44);			/* add header part */
//				json_send_ptr += 44;	
//				response_tank_pump_settings(UGT_tank.tank_config_ptr->tank_num);	  //pending
//			}
		break;

		case GET_PUMP_STATUS:
			get_pump_status(tank_ptr);
		break;

//		case SEND_NOTIFICATION:
//			create_notification_packet(tank_ptr,param);		
//		break;

		case SEND_CURRENT_WATER_LEVEL:
		//	get_tank_status(tank_ptr);
		break;

		case WMS_GUI_SEND_HOURLY_CONSUMPTION:
		case GET_HOURLY_CONSUMPTION:
			ADD_START_BRACE;
			add_field_name(field_name_indx++);				/* add tank number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(tank_ptr->tank_config_ptr->tank_num, json_send_ptr);
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;
		
			add_field_name(field_name_indx++);				/* add hour number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(RTC_TimeStructure.RTC_Hours, json_send_ptr);
			json_send_ptr += len; 		
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;	

			add_field_name(field_name_indx++);				/* add consumption */
			ADD_DOUBLE_QUOTE;
			if(tank_ptr->tank_config_ptr->tank_num <= 25)
				len = dec_ascii_arr(current_hour_oht_consumption[RTC_TimeStructure.RTC_Hours], json_send_ptr);
			else
				len = dec_ascii_arr(current_hour_ugt_consumption[RTC_TimeStructure.RTC_Hours], json_send_ptr); 
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;		
			ADD_CLOSING_BRACE;	
			if(wms_sys_config.total_ugt > 0){	
				field_name_indx = 1;
				ADD_CLOSING_BRACE;
				memcpy(json_send_ptr, json_send_arr, 44);			/* add header part */
				json_send_ptr += 44;
				ADD_START_BRACE;
				add_field_name(field_name_indx++);				/* add tank number */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(26, json_send_ptr);
				json_send_ptr += len; 
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;
			
				add_field_name(field_name_indx++);				/* add hour number */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(RTC_TimeStructure.RTC_Hours, json_send_ptr);
				json_send_ptr += len; 		
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;	
	
				add_field_name(field_name_indx++);				/* add consumption */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(current_hour_ugt_consumption[RTC_TimeStructure.RTC_Hours], json_send_ptr); 
				json_send_ptr += len; 
				ADD_DOUBLE_QUOTE;		
				ADD_CLOSING_BRACE;	
			}


		break;

		case GET_HOURLY_AVAILABILITY:
		case WMS_GUI_SEND_HOURLY_AVAILABILITY:
			ADD_START_BRACE;
			add_field_name(field_name_indx++);				/* add tank number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(tank_ptr->tank_config_ptr->tank_num, json_send_ptr);
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;
		
			add_field_name(field_name_indx++);				/* add hour number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(RTC_TimeStructure.RTC_Hours, json_send_ptr);
			json_send_ptr += len; 		
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;	

			add_field_name(field_name_indx++);				/* add availability */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(tank_ptr->current_level, json_send_ptr);
			json_send_ptr += len; 		
			ADD_DOUBLE_QUOTE;
			ADD_CLOSING_BRACE;	
			if(wms_sys_config.total_ugt > 0){	
				field_name_indx = 1;
				ADD_CLOSING_BRACE;
				memcpy(json_send_ptr, json_send_arr, 44);			/* add header part */
				json_send_ptr += 44;
				ADD_START_BRACE;
				add_field_name(field_name_indx++);				/* add tank number */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(26, json_send_ptr);
				json_send_ptr += len; 
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;
			
				add_field_name(field_name_indx++);				/* add hour number */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(RTC_TimeStructure.RTC_Hours, json_send_ptr);
				json_send_ptr += len; 		
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;	
	
				add_field_name(field_name_indx++);				/* add consumption */
				ADD_DOUBLE_QUOTE;
				len = dec_ascii_arr(UGT_tank.current_level, json_send_ptr); 
				json_send_ptr += len; 
				ADD_DOUBLE_QUOTE;		
				ADD_CLOSING_BRACE;	
			}
	


		break;

		case GET_EVENT_LOG:
		//	get_event_log(param);
		break;

		case SEND_NOTIFICATION:
		
	//	case GET_NOTIFICATION:
			ADD_START_BRACE;
			add_field_name(field_name_indx++);				/* add tank number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(*param++, json_send_ptr);
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;
		
			add_field_name(field_name_indx++);				/* add notification number */
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(*param, json_send_ptr);
			json_send_ptr += len; 		
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;	
			
			add_field_name(field_name_indx++);				/* add sub field if any */
			ADD_DOUBLE_QUOTE;
			switch(*param){
				case 1:
					for(temp = 0; temp < 32 ; temp++){             //notification field
						if( ((sensor_malfunctioning_notification_field >> temp) & (0x01) ) == 1) {
							count = temp;
							len = dec_ascii_arr(++count, json_send_ptr);
							json_send_ptr += len; 
							*(json_send_ptr++) = '-' ;
						}
					}
					json_send_ptr--;
					param += 8;
				break;

				default:
					param++;
//					len = dec_ascii_arr(*param, json_send_ptr);    
//					json_send_ptr += len;       //atul comment
					*(json_send_ptr++) = 0x30;
				//	len = dec_ascii_arr(*param, json_send_ptr);
				//	json_send_ptr += len; 
					param += 7;
				break;
			}
		  ADD_DOUBLE_QUOTE;
			ADD_COMMA;	

		  add_field_name(field_name_indx++);				/* add time stamping */
			ADD_DOUBLE_QUOTE;
			for(count = 0; count < 6; count++){
				dec_ascii_byte((*param++), json_send_ptr, 2);
				json_send_ptr += 2; 
				*(json_send_ptr++) = '-' ;
			}
			json_send_ptr--;
			ADD_DOUBLE_QUOTE;
			ADD_CLOSING_BRACE;		
	
		break;

		case GET_NOTIFICATION:
		//	get_notification_log(param);
		break;
	
		case GET_CURRENT_WATER_LEVEL_PUMP_STATUS:
			get_tank_pump_status();
		//	create_get_tank_level_packet();	
		break;

		case GET_CURRENT_WATER_LEVEL:
			create_get_tank_level_packet();	
		break;



//		case SEND_LICENSE_NUM:
//			add_field_val(param, 24);	
//		break;

		case GUI_WMS_GET_TANK_SCHEDULE:
			get_tank_schedule(*param);
		break;

		case WMS_GUI_GET_WATER_CONSUMPTION:
		//	get_consumption_log(param);
		break;
		
		case GET_FW_VERSION_NUMBER:
			ADD_DOUBLE_QUOTE;
			for(count = 0; count < 3; count++){
				memcpy(json_send_ptr, &Neotech_Header[6 + count*2], 2);
			//	len = dec_ascii_arr(system_info.fw_ver[count], json_send_ptr);
			//	json_send_ptr += len; 
				json_send_ptr += 2;
				*(json_send_ptr++) = '.' ;	
			}
			json_send_ptr--;
			ADD_DOUBLE_QUOTE;
		break;
		

		case GET_HW_VERSION_NUMBER:
			ADD_DOUBLE_QUOTE;
			for(count = 0; count < 3; count++){
				len = dec_ascii_arr(HwVersion[count], json_send_ptr);
				json_send_ptr += len; 
				*(json_send_ptr++) = '.' ;	
			}
			json_send_ptr--;
			ADD_DOUBLE_QUOTE;
		break;			
		

		case FIRMWARE_UPGRADE:
			add_field_val(param, 1);
		break;

		case BROADCAST_IP_ADDRSS:
			ADD_DOUBLE_QUOTE;
			for(count = 0; count < 4; count++){
		//		dec_ascii_byte(localm[NETIF_ETH].IpAdr[count], json_send_ptr, 3);
				json_send_ptr += 3;
				*(json_send_ptr++) = '.' ;	
			}
			json_send_ptr--;
			ADD_DOUBLE_QUOTE;
		break;

		case GUI_WMS_ADD_LICENSE:
			add_field_val(param, 1);
		break;

		case GUI_WMS_GET_LICENSE_NUM:
			add_field_val((uint8_t *)&system_info.license_num[0], 24);	
		break;

		case GUI_WMS_ADD_SERIAL_NUM:
			add_field_val(param, 1);
		break;

		case GUI_WMS_GET_SERIAL_NUM:
			add_field_val((uint8_t *)&system_info.serial_num[0], SERIAL_NUM_LEN);	
		break;

		case GUI_WMS_ADD_MAC_ID:
			add_field_val(param, 1);
		break;

		case GUI_WMS_GET_MAC_ID:
			add_field_val((uint8_t *)&system_info.mac_id[0], 12);	
		break;
		
		case GET_SERVER_LICENSE_STATUS:
			if(*param == 0x31){	
				ADD_START_BRACE;
				add_field_name(field_name_indx++);
				ADD_DOUBLE_QUOTE;
				*json_send_ptr++ = '0';				
				ADD_DOUBLE_QUOTE;
				ADD_COMMA;

				add_field_name(field_name_indx++);				
				ADD_DOUBLE_QUOTE;
				strncpy((char *)json_send_ptr, (char *)&system_info.serial_num[0], SERIAL_NUM_LEN - 2);
				json_send_ptr += (SERIAL_NUM_LEN - 2); 
				ADD_DOUBLE_QUOTE;
				ADD_CLOSING_BRACE;		
			}
			else{
				*param = 0x31;
				add_field_val((uint8_t *)param, 1);	
	
			}
		break;

		case SEND_EVENT_LOG:
			  			/* create a packet */
			ADD_START_BRACE;
			add_field_name(field_name_indx++);			
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(param[1], json_send_ptr);		/* add tank number */
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;	
			ADD_COMMA;	

			add_field_name(field_name_indx++);			
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(param[0], json_send_ptr);			/* add event type */
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;	
			ADD_COMMA;	

			add_field_name(field_name_indx++);			
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(param[2], json_send_ptr);			/* add sensor num/pump status */
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;	
			ADD_COMMA;	

			add_field_name(field_name_indx++);			
			ADD_DOUBLE_QUOTE;
			len = dec_ascii_arr(param[3], json_send_ptr);			/* add pump cause/sensor number */
			json_send_ptr += len; 
			ADD_DOUBLE_QUOTE;
			ADD_COMMA;		

		   	add_field_name(field_name_indx++);				/* add time stamping */
			ADD_DOUBLE_QUOTE;
			//temp_arr[9]	= 0;							/* sec */
			for(count = 0; count < 6; count++){
				dec_ascii_byte(param[4 + count], json_send_ptr, 2);
				json_send_ptr += 2; 
				*(json_send_ptr++) = '-' ;
			}
			json_send_ptr--;
			ADD_DOUBLE_QUOTE;
			ADD_CLOSING_BRACE;
		break;



	}
	ADD_CLOSING_BRACE;
	ADD_NULL_CHAR;
}


/************************************************************************//**
*	void add_field_name(uint8_t field_num)
*
* @brief			This routine adds the field name.
*				
*
* @param 			field_num - field name to be inserted in the send array.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/

void add_field_name(uint8_t field_num){
	
	*(json_send_ptr++) = '"';
	 dec_ascii_byte(field_num, json_send_ptr, 2);
	 json_send_ptr += 2;
	 *json_send_ptr++ = '"'; 
	 *json_send_ptr++ = ':'; 

}
/************************************************************************//**
*	uint8_t extract_json_field_val(uint8_t *ptr, uint8_t *json_payload)
*
* @brief			This routine extracts the field value from packet.
*				
*
* @param 			*ptr - pointer to buffer - insert extracted bytes in this buffer.
*					*json_payload- pointer to buffer from which the values to be extracted.
*
* @returns			len - len of extracted bytes.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
****************************************************************************/
uint8_t extract_json_field_val(uint8_t *ptr, uint8_t *json_payload){
	uint8_t len = 0;

	json_payload++;
	while(*json_payload != '"'){
		*ptr++ = *json_payload++;		
		len++;
	}
	return len;
}

/************************************************************************//**
*	uint8_t copy_json_field_val(uint8_t *json_payload){
*
* @brief			This function copies the JSON field value to JSON send array.
	
*
* @param 			*json_payload - pointer to buffer containing data.
*
* @returns			len	- len of copied data.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
uint8_t copy_json_field_val(uint8_t *json_payload){
	uint8_t len = 0, loc = 0;

	while(loc != 2){
		*json_send_ptr++ = *json_payload;
		if(*json_payload == '"')
			loc++;
		json_payload++;
		len++;
	}
	return len;

}

/************************************************************************//**
*		void add_json_header(uint32_t command){
*
* @brief			This function initialize the send buffer with passed command number.
	
*
* @param 			command- command to be added in the JSON array.
*
* @returns			None.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note				None.                                            
****************************************************************************/
void add_json_header(uint32_t command){
	
	strcpy((char *)json_send_ptr, g_send_ptr);
	json_send_ptr += 31;
   	dec_ascii_byte(command, json_send_ptr, 4);
	json_send_ptr += 13;

}

/***************************************************************************************//**
*	uint8_t JSON_GUI_packet_process(struct json_struct *wms_payload, uint16_t total_byte_packet)
*
* @brief			This routine process the packet comes from GUI.
*				
*
* @param 			struct json_struct *wms_payload - pointer to buffer containing data.
*					total_byte_packet - len of data
*
* @returns			1 - if packet validates.
*					0 - otherwise
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
uint16_t JSON_GUI_packet_process(struct json_struct *wms_payload, uint16_t total_byte_packet){
	uint8_t tank_num, temp_status = 0x30 ,read_arr[20], len, count, loc_status, temp_arr[50], pos, loc_var[5], loc_byte;
	uint8_t *ptr, counter, ret_val, tank_pump_setting_configured;
	uint16_t ret = JSON_INVALID, cmnd_loc;
	uint8_t field_name_indx = 1, nob, loc = 0, arr[4], temp = 0;

	switch(wms_payload->cmd){

		case GUI_WMS_AUTHENTICATE_MAC_ID:
			if(total_byte_packet > 5){				// check only for client 
				cmnd_loc = wms_payload->cmd;
				temp_status = authenticate_MAC_ID(wms_payload, total_byte_packet);
				create_GUI_tx_packet(cmnd_loc, 0, &temp_status);
			//	ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
				ret = PACKET_VALIDATED;		
			}
		break; 
			
		case BUYERS_REGISTRATION:
				temp_status =	Buyers_Registration_process(wms_payload);
				create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
				ret = PACKET_VALIDATED;		
		break; 
			

		case GUI_WMS_DEVICE_REGISTRATION:
			temp_status = device_registration(wms_payload);
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			/*add user name  */
//			if(status == '0')
//				send_cloud_server_status |= 0x01;  
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
		//	ret = PACKET_VALIDATED;	
		break;

		case GUI_WMS_VIEW_DEVICE_REGISTRATION_LIST:
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;	
		break;

		case GUI_WMS_DEVICE_REGISTRATION_REMOVE:
	   		temp_status = remove_MAC_ID(wms_payload, total_byte_packet);
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
//			if(status == '0')
//				send_cloud_server_status |= 0x01;  
			ret = PACKET_VALIDATED;
//			ret = GUI_WMS_DEVICE_REGISTRATION_REMOVE;
		break;

		case GUI_WMS_SAVE_DATE_TIME:		 
			temp_status = update_GUI_time(wms_payload, total_byte_packet - 6);
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
		break;				 
		
		case GUI_WMS_GET_DATE_TIME:
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = PACKET_VALIDATED;
		break;
//
//		case SET_NW_SETTING:
//			temp_status = set_nw_settings(wms_payload, total_byte_packet - 6);
//			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
//			ret = PACKET_VALIDATED;
//		break;
//
//		case GET_NW_SETTING:
//			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
//			ret = PACKET_VALIDATED;
//		break;
//
		case GUI_WMS_DEVICE_PASSWORD_AUTHENTICATE:
			//dflash_read_multiple_byte(ADMIN_PASSWRD_INFO_ADDR, &read_arr[0], ADMIN_PASSWRD_LEN);
		
			eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), READ_OP, &read_arr[0], ADMIN_USER_PWD_MAXLEN);
			if(strncmp((char *)&wms_payload->data[1],(char *)&read_arr[0], ADMIN_USER_PWD_MAXLEN) == 0)
				temp_status = '0';
			else
				temp_status = '1';
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;	
		break;

		case GUI_WMS_DEVICE_PASSWORD_CHANGE:
			//dflash_read_multiple_byte(ADMIN_PASSWRD_INFO_ADDR, &read_arr[0], ADMIN_PASSWRD_LEN);
			eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), READ_OP, &read_arr[0], ADMIN_USER_PWD_MAXLEN);
			if(strncmp((char *)&wms_payload->data[1],(char *)&read_arr[0], ADMIN_USER_PWD_MAXLEN) == 0){
			//	dflash_write_multiple_byte(ADMIN_PASSWRD_INFO_ADDR, &wms_payload->data[9], ADMIN_USER_PWD_MAXLEN);
				eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), WRITE_OP, &wms_payload->data[15], ADMIN_USER_PWD_MAXLEN);
			//	eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), READ_OP, &read_arr[0], ADMIN_USER_PWD_MAXLEN);
				temp_status = '0';
			}
			else
				temp_status = '1';
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;	
		break;

		case GUI_WMS_DEVICE_PASSWORD_FORGET:
			//dflash_read_multiple_byte(LICENSE_NUM_INFO_ADDR, &read_arr[0], LICENSE_NUM_LEN);
			eeprom_data_read_write(LICENSE_NUM_INFO_ADDR, READ_OP, &read_arr[0], 24);
			if(strncmp((char *)&wms_payload->data[1],(char *)&read_arr[0], LICENSE_NUM_LEN) == 0){
			//	dflash_write_multiple_byte(ADMIN_PASSWRD_INFO_ADDR, &wms_payload->data[LICENSE_NUM_LEN + 3], ADMIN_PASSWRD_LEN);
				eeprom_data_read_write((DIFF_USER_TYPE_START_ADDR(ADMIN_USER_TYPE) + 11), WRITE_OP, &wms_payload->data[27], ADMIN_USER_PWD_MAXLEN);
				temp_status = '0';
			}
			else{
			 temp_status = '1';
			}
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;	
		break;

		case GUI_WMS_SET_BUZZER_STATE:
			save_buzzer_setting(wms_payload, total_byte_packet - 6);
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = 1;	
		break;

		case GUI_WMS_GET_BUZZER_STATE:
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = PACKET_VALIDATED;	
		break;

	
		case SET_TANK_PUMP_SENSOR_SETTINGS:
 			len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);
			tank_num = ascii_decimal(&read_arr[0], len);
			save_sensor_settings(wms_payload, total_byte_packet - 6);	   //pending
			create_copy_response(wms_payload, total_byte_packet);
			tank_pump_setting_configured = 1;
			eeprom_data_read_write(TANK_PUMP_SENSOR_SETTING_ADDR, WRITE_OP,&tank_pump_setting_configured, 1);
//			if(tank_num > 0 && tank_num <= 25 ){
//				create_GUI_tx_packet(wms_payload->cmd, &OHT_tank[0], 0);
//			}
//			else if(tank_num > 25 && tank_num < 50 ){
//				create_GUI_tx_packet(wms_payload->cmd, &UGT_tank, 0);
//			}
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
		break;

		case GET_TANK_PUMP_SENSOR_SETTINGS:
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
		//	get_tank_pump_settings();
		//	*(json_send_ptr++) = '\0';
			ret = PACKET_VALIDATED;	
		break;
		
		case PUMP_ON:
			pump_on_off_flag = 1;
	 		pump_on_count++;
			len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);
			tank_num = ascii_decimal(&read_arr[0], len);	
			if(tank_num >= 1 && tank_num < 26){
				if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
					if(status.oht_pump_ptr->pump_state == 0){
				//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
						
						//	if(!(chk_exception_schedule(OHT_tank[0].tank_config_ptr->tank_num))){
								if((ret_val = chk_automatic_task_status(tank_num, ON)) == 0){
									actuate_motor(OHT, 1);
									last_pump_triggered_time_OHT = RTC_TimeStructure;
									last_pump_triggered_date_OHT = date_obj;
									OHT_triggered = 1;
									event_info |= (1 << OHT_PUMP_TRIGGER); 
									oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_GUI;
								}
								else{
									system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
									oht_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
									notification_info_oht();
									break;
								}
						//	}
						//	else;	// due to exception schedule 
						}
//					else{
//						oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//					}
				
				}
				else {
							oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
							notification_info_oht();
							break;
				}
			//	else{
					create_GUI_tx_packet(GET_PUMP_STATUS,&OHT_tank[0], 0);
					ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
			//	}
			}
//			else if(tank_num == 26 && wms_sys_config.ugt_pump_select == 1){
//				if(status.ugt_pump_ptr->pump_state == 0){
//				//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//						if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//						//	if(!(chk_exception_schedule(UGT_tank.tank_config_ptr->tank_num))){
//								if((ret_val = chk_automatic_task_status(tank_num, ON)) == 0){
//									actuate_motor(UGT, 1);
//									last_pump_triggered_time_UGT = RTC_TimeStructure;
//									last_pump_triggered_date_UGT = date_obj;
//									UGT_triggered = 1;
//									event_info |= (1 << UGT_PUMP_TRIGGER); 
//									ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_GUI;
//								}
//								else{
//									system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task							
//								}
//						//	}
//						//	else;
//								// due to exception schedule 
//						}
//						else
//							ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
//				//	}
////					else{
////						ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
////					}
//				
//				}
//			//	else{
//					create_GUI_tx_packet(GET_PUMP_STATUS,&UGT_tank, 0);
//					ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
//			//	}
//			}				
		break;

		case PUMP_OFF:
			pump_on_off_flag = 1;
			pump_off_count++;
			len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);
			tank_num = ascii_decimal(&read_arr[0], len);	
			if(tank_num >= 1 && tank_num < 26){
				
				//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
					if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
						if(status.oht_pump_ptr->pump_state == 1){
						//	if(!(chk_exception_schedule(OHT_tank[0].tank_config_ptr->tank_num))){
								if((ret_val = chk_automatic_task_status(tank_num, OFF)) == 0){
									actuate_motor(OHT, 0);
									last_pump_triggered_time_OHT = RTC_TimeStructure;
									last_pump_triggered_date_OHT = date_obj;
									OHT_triggered = 1;
									event_info |= (1 << OHT_PUMP_TRIGGER); 
									oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_GUI;
								}
								else {
									system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task	
									oht_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
									notification_info_oht();
									break;
								}
						//	}
						//	else;
								// due to exception schedule 
						}
						
//////						if(status.oht_pump_ptr->pump_state == 0){
//////								if((ret_val = chk_automatic_task_status(tank_num, ON)) == 8){
//////								system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task	
//////								oht_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
//////								notification_info_oht();
//////								break;
//////								}
//////						}
						
						
//					else{
//						oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//					}
				
					}
					else {
							oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
							notification_info_oht();
							break;
					 }
			//	else{
					create_GUI_tx_packet(GET_PUMP_STATUS,&OHT_tank[0], 0);
					ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
			//	}
			}
//			else if(tank_num == 26 && wms_sys_config.ugt_pump_select == 1){
//				if(status.ugt_pump_ptr->pump_state == 1){
//				//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//						if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//						//	if(!(chk_exception_schedule(UGT_tank.tank_config_ptr->tank_num))){
//								if((ret_val = chk_automatic_task_status(tank_num, OFF)) == 0){
//									actuate_motor(UGT, 0);
//									last_pump_triggered_time_UGT = RTC_TimeStructure;
//									last_pump_triggered_date_UGT = date_obj;
//									UGT_triggered = 1;
//									event_info |= (1 << UGT_PUMP_TRIGGER); 
//									ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_GUI;
//								}
//								else
//									system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task																
//						//	}		
//						//	else;
//								// due to exception schedule 
//						}
//						else
//							ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
////					}
////					else{
////						ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
////					}
//				
//				}
//		//		else{
//					create_GUI_tx_packet(GET_PUMP_STATUS,&UGT_tank, 0);
//					ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
//		//		}
//			}				
		break;



		case GET_PUMP_STATUS:
		
		break;

		case SAVE_WATER_USAGE_SETTING:		//set automated task
			automated_task_enable(wms_payload, total_byte_packet - 6);
			create_copy_response(wms_payload, total_byte_packet);
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;		
		break;


		case GET_WATER_USAGE_SETTING:	 //get automated task
			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->data[0]);
		//	get_automated_task_status(wms_payload, total_byte_packet - 6);
		//	get_automated_task_status(param);
			ret = PACKET_VALIDATED;
		break;

//		case GET_NOTIFICATION:				//atul
//			pump_on_off_flag =  0;
//			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->idx);
//			ret = PACKET_VALIDATED;
//		break;

//		case GET_EVENT_LOG:						//atul
//			pump_on_off_flag = 0;
//			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->idx);
//			ret = PACKET_VALIDATED;
//		break;

		case GET_CURRENT_WATER_LEVEL_PUMP_STATUS:				
			pump_on_off_flag = 0;
			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->data[0]);
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
		break;

		case GET_CURRENT_WATER_LEVEL:
			pump_on_off_flag = 0;
			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->data[0]);
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
		break;

//		case GET_HOURLY_CONSUMPTION:			//atul
//			create_GUI_tx_packet(GET_HOURLY_CONSUMPTION, &OHT_tank[0], 0);
//			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;		
//		break;

//		case GET_HOURLY_AVAILABILITY:
//			create_GUI_tx_packet(GET_HOURLY_AVAILABILITY, &OHT_tank[0], 0);
//			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;		
//		break;

		 case GUI_WMS_SAVE_TANK_SCHEDULE:			//atul
			if((loc_status = save_schedules (wms_payload)) != 0){	  // error
				switch(loc_status){
					case SCHEDULE_SAVE_ERROR:
					case SCHEDULE_DISABLE_ERROR:
					case SCHEDULE_DELETE_ERROR:
						json_send_ptr = &json_send_arr[0];
						strncpy((char *)json_send_ptr, (char *)error_code, 31);
						json_send_ptr += 31;
						len = dec_ascii_arr(loc_status, json_send_ptr);
						json_send_ptr += len; 
						ADD_DOUBLE_QUOTE;
						ADD_CLOSING_BRACE;
						ADD_NULL_CHAR;
					break;





					default:			// save successfully and re
						intit_JSON_send_header();
						dec_ascii_byte(wms_payload->cmd, arr, 4);
						strncpy((char *)json_send_ptr, (const char *)arr, 4);		/* Add command field */
						json_send_ptr += 13;										/* Jump pointer to payload part */
						*(json_send_ptr++) = '{';
						while(loc != total_byte_packet){
							add_field_name(field_name_indx++);
							if(temp == 1){
								*(json_send_ptr++) = '"';
								*(json_send_ptr++) = loc_status + 0x30;
								*(json_send_ptr++) = '"';
								loc_byte = extract_json_field_val(loc_var, &wms_payload->data[loc]);
								loc += (loc_byte + 2);
							}
							else{
								nob = copy_json_field_val((uint8_t *)&wms_payload->data[loc]);
								loc += nob;
							}
							*(json_send_ptr++) = ',';
							temp++;
							
						}
						json_send_ptr--;
						*(json_send_ptr++) = '}';
						*(json_send_ptr++) = '}';
						*(json_send_ptr++) = '\0';
					break;
				}										
			}
			else{
				create_copy_response(wms_payload, total_byte_packet);
			}
			ret = PACKET_VALIDATED_RESPONSE_BROADCAST;
	   	break;

		case GUI_WMS_GET_TANK_SCHEDULE:
			#ifdef FW_VER_GREATER_THAN_3_7_25
				len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);
				tank_num = ascii_decimal(&read_arr[0], len);	
				create_GUI_tx_packet(GUI_WMS_GET_TANK_SCHEDULE, 0, &tank_num);
			#else
				create_GUI_tx_packet(GUI_WMS_GET_TANK_SCHEDULE, 0, 0);
			#endif
			ret = PACKET_VALIDATED;
		break;

//		case WMS_GUI_GET_WATER_CONSUMPTION:
//			create_GUI_tx_packet(wms_payload->cmd, 0, (uint8_t *)&wms_payload->idx);
//			ret = PACKET_VALIDATED;
//		break;
//
//		
		case GET_FW_VERSION_NUMBER:		
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = PACKET_VALIDATED;
		break;
		
		
		case GET_HW_VERSION_NUMBER:		
			create_GUI_tx_packet(wms_payload->cmd, 0, 0);
			ret = PACKET_VALIDATED;
		break;
		
//
//		case FIRMWARE_UPGRADE:
//		  	loc = 1;
//			count = 0;
//		  // if crc match then status 0
//		  // extract crc and version number
//		  	len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);		// extract crc
//		  	
//			len = extract_json_field_val(&read_arr[0], &wms_payload->data[len + 2]);		// extract version number
//			ptr = &read_arr[count];							
//
//			ptr = strchr((char *)&read_arr[count],'.');
//			pos = ptr - &read_arr[count]; 
//		   	temp_arr[0] = ascii_decimal(&read_arr[count], pos);
//			count += (pos + 1);
//			
//			ptr =strchr((char *)&read_arr[count],'.');
//			pos = ptr - &read_arr[count]; 
//			temp_arr[1] = ascii_decimal(&read_arr[count], pos);			
//			count += (pos + 1);		  
//
//			 
//			temp_arr[2] = ascii_decimal(&read_arr[count], len - count);			
//		  	
//				
//			dflash_write_multiple_byte(WMS_NEW_FIRMWARE_PRESENT_ADDRESS, &loc, 1);     		// by default oht TANK enable
//			dflash_write_multiple_byte(UPGRADING_FW_ADDR, temp_arr, 3);
//		  	loc = 0x30;
//		  	create_GUI_tx_packet(wms_payload->cmd, 0, &loc);
//		  // NVIC_SystemReset();
//		  	firmware_upgrade_flag = 1;
//		  	ret = PACKET_VALIDATED;
//		break;

		case GUI_WMS_ADD_LICENSE:
			len = extract_json_field_val(&temp_arr[0], &wms_payload->data[0]);
			if(check_licencse_authenticate_logic((char *)&wms_payload->data[1], (char *)&system_info.serial_num[0], (char *)&system_info.mac_id[0]) == 1){
		//	if(len == 24){
				if(license_type == 1){		   // gui client
				//	dflash_write_multiple_byte(LICENSE_NUM_INFO_ADDR, &temp_arr[0], 24);	   
					eeprom_data_read_write(LICENSE_NUM_INFO_ADDR, WRITE_OP, &temp_arr[0], 24); /* write license num */
				//	dflash_read_multiple_byte (LICENSE_NUM_INFO_ADDR, &system_info.license_num[0], 24);
					eeprom_data_read_write(LICENSE_NUM_INFO_ADDR, READ_OP, &system_info.license_num[0], 24);
				//	dflash_write_multiple_byte(TOTAL_REG_KEY_ADDR, &total_client_license, 1);	   
					eeprom_data_read_write(TOTAL_REG_KEY_ADDR, WRITE_OP, &total_client_license, 1);	/* write license num */
					total_client_license = license_class;
				}
				else if(license_type == 51){				 // server
					temp_status = 1;
				//	dflash_write_multiple_byte(SERVER_LICENSE_ADDR, &temp_status, 1);
					eeprom_data_read_write(SERVER_LICENSE_ADDR, WRITE_OP, &temp_status, 1);	
				//	dflash_write_multiple_byte(SERVER_LICENSE_ADDR + 1, (char *)&wms_payload->data[1], LICENSE_NUM_LEN);
				    eeprom_data_read_write(SERVER_LICENSE_ADDR + 1, WRITE_OP, (char *)&wms_payload->data[1], LICENSE_NUM_LEN);
				}
				temp_status = 0x30;
			}
			else
				temp_status = 0x31;
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
		break;

	   case GUI_WMS_GET_LICENSE_NUM:
	  		create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
	   break;

   		case GUI_WMS_ADD_SERIAL_NUM:
			len = extract_json_field_val(&temp_arr[0], &wms_payload->data[0]);
			if(len == 12){
			//	dflash_write_multiple_byte(SERIAL_NUM_INFO_ADDR, &temp_arr[0], 14);	   
				eeprom_data_read_write(SERIAL_NUM_INFO_ADDR, WRITE_OP, &temp_arr[0], SERIAL_NUM_LEN);	/* write serial num */
			//	dflash_read_multiple_byte (SERIAL_NUM_INFO_ADDR, &system_info.serial_num[0], SERIAL_NUM_LEN);
				eeprom_data_read_write(SERIAL_NUM_INFO_ADDR, READ_OP, &system_info.serial_num[0], SERIAL_NUM_LEN);
				temp_status = 0x30;
			}
			else
				temp_status = 0x31;
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
		break;

	   case GUI_WMS_GET_SERIAL_NUM:
	  		create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
	   break;

	   	case GUI_WMS_ADD_MAC_ID:
			len = extract_json_field_val(&temp_arr[0], &wms_payload->data[0]);
			if(len == 12){
				for(temp = 0; temp < 12; temp++){
					if(temp_arr[temp] > '9'){
						if(temp_arr[temp] >= 97)
							temp_arr[temp] -= 87;
						else
							temp_arr[temp] -= 55;
					}
					else
						temp_arr[temp] -= 48;			
				}
				for(counter = 0, temp = 0 ; counter < 12; temp++, counter += 2){
					read_arr[temp] = temp_arr[counter];
					read_arr[temp] <<= 4;
					read_arr[temp] = (read_arr[temp] | ((temp_arr[counter + 1]) & 0x0f));
		 		}				

			//	dflash_write_multiple_byte(SYSTEM_MAC_ID_INFO_ADDR, &read_arr[0], 6);	   
				eeprom_data_read_write(SYSTEM_MAC_ID_INFO_ADDR, WRITE_OP, &read_arr[0], 6);	 /* write serial num */
			//	dflash_read_multiple_byte (SYSTEM_MAC_ID_INFO_ADDR, &read_arr[0], 6);	
				eeprom_data_read_write(SYSTEM_MAC_ID_INFO_ADDR, READ_OP, &read_arr[0], 6);   /* system mac id */
				for(count = 0, temp = 0;count < 6; count++, temp += 2){
					system_info.mac_id[temp] =	 ((read_arr[count] & 0xf0) >> 4) + 0x30;
					system_info.mac_id[temp + 1] =	 (read_arr[count] & 0x0f) + 0x30;
				}
				for(temp = 0; temp < 12; temp++){
					if(system_info.mac_id[temp] > 0x39){
						system_info.mac_id[temp] += 7;
					}
				}
				temp_status = 0x30;
			}
			else
				temp_status = 0x31;
			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
		break;

	 	case GUI_WMS_GET_MAC_ID:
	  	create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
			ret = PACKET_VALIDATED;
	   break;  //atul comment end

//	   	case GUI_ENABLE_BROADCAST:
//			len = extract_json_field_val(&read_arr[0], &wms_payload->data[0]);	
//			udp_broadcast_counter = ascii_decimal(read_arr, len);	
//			udp_broadcast_state = SOFT_SWITCH;
//			udp_broadcast_flag = 1;								
//		break; 

//		case 5000:  /* temp */
//			tsk_lock();
//			loc	= 'N';
//			dflash_write_multiple_byte (SYSTEM_INFO_ADDR, &loc, 1);		   	/* board status */
//			loc = '\0';
//			for(count = 0; count < TOTAL_REG_KEY; count++){
//				dflash_write_multiple_byte(DEVICE_REG_START_INFO_ADDR + count*DEVICE_REG_LEN , &loc, 1);
//			}
//			write_default_configuration();
//			restore_configured_settings();
//			clear_consumption_logs();
//			tsk_unlock();
//		break;	
		
//		case GET_SERVER_LICENSE_STATUS:	
//			dflash_read_multiple_byte(SERVER_LICENSE_ADDR, &temp_status, 1);
//			temp_status += 0x30;
//			create_GUI_tx_packet(wms_payload->cmd, 0, &temp_status);
//			ret = PACKET_VALIDATED;
//			
//		break;	

 	}
	return ret;

}


/***************************************************************************************//**
*		uint16_t JSON_server_packet_validates(){
*
* @brief			this function validates the expression in JSON format.
*				
*
* @param 			none 
*
* @returns			1 - if packet validates.
*					0 - otherwise
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/

uint16_t JSON_server_packet_validates(serial_port *nw_port){
	uint16_t nob = 0;
	uint16_t ret_status = 0;  

	if(nw_port->RxGetPtr != nw_port->RxPutPtr){
		

		switch(expression_validate(nw_port, &nob)){
			case  EXPRESSION_VALIDATE:
				ret_status = JSON_GUI_packet_process(&json_info, nob);
				//	nw_port->RxGetPtr = nw_port->RxPutPtr;
			break;

			case  EXPRESSION_INCOMPLETE:
				nw_port->RxGetPtr = nw_port->RxPutPtr;
				ret_status = JSON_INVALID;
			//	json_client.RxGetPtr++;	
			break;

			case  SERVER_ID_NOT_FOUND:
				ret_status = JSON_INVALID;
				nw_port->RxGetPtr = nw_port->RxPutPtr;	
			break;

			default:
				ret_status = JSON_INVALID;
				nw_port->RxGetPtr = nw_port->RxPutPtr;
			break;
		}
		
	}
	return ret_status;
}
/***************************************************************************************//**
*		void intit_JSON_send_header(){
*
* @brief			This routine initalize the JSON send array.
*				
*
* @param 			none 
*
* @returns			None
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
void intit_JSON_send_header(){
	
	json_send_ptr = &json_send_arr[32];
	strcpy((char *)json_send_arr, g_send_ptr);
		
}	
/***************************************************************************************//**
*		void add_field_val(uint8_t *ptr, uint8_t len){
*
* @brief			This routine adds the field value.
*				
*
* @param 			*ptr - pointer to buffer containing data. 
*	 				len - length of field value
*
* @returns			None
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
void add_field_val(uint8_t *ptr, uint8_t len){
	uint8_t count;
		
	*(json_send_ptr++) = '"';
	for(count = 0 ; count < len; count++){
		*(json_send_ptr++) = *ptr++;
	}
	*(json_send_ptr++) = '"';

}								  


/***************************************************************************************//**
*		void create_copy_response(struct json_struct *wms_payload_info, uint8_t num_bytes){
*
* @brief			This routine copy the packet to send buffer.
*				
*
* @param 			struct json_struct *wms_payload_info - pointer to structure from where copy the data
*					num_bytes 						- number of bytes to be copied.
*
* @returns			None
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
void create_copy_response(struct json_struct *wms_payload_info, uint16_t num_bytes){
	uint8_t field_name_indx = 1, nob, loc = 0, arr[4];

	intit_JSON_send_header();
	dec_ascii_byte(wms_payload_info->cmd, arr, 4);
	strncpy((char *)json_send_ptr, (const char *)arr, 4);		/* Add command field */
	json_send_ptr += 13;										/* Jump pointer to payload part */

	*(json_send_ptr++) = '{';
	while(loc != num_bytes){
		add_field_name(field_name_indx++);
		nob = copy_json_field_val((uint8_t *)&wms_payload_info->data[loc]);
		*(json_send_ptr++) = ',';
		loc += nob;
	}
	json_send_ptr--;
	*(json_send_ptr++) = '}';
	*(json_send_ptr++) = '}';
	*(json_send_ptr++) = '\0';
}


void get_nw_settings(void){
	uint8_t field_name_indx = 1, loc;

	ADD_START_BRACE;
	add_field_name(field_name_indx++);
	ADD_DOUBLE_QUOTE;
	dec_ascii_byte(system_info.dhcp_flag,json_send_ptr++,  1);
	ADD_DOUBLE_QUOTE;
	ADD_COMMA;

	add_field_name(field_name_indx++);
	ADD_DOUBLE_QUOTE;
	for(loc = 0; loc < 4; loc++){
	//	dec_ascii_byte(localm[NETIF_ETH].IpAdr[loc], json_send_ptr, 3);
		json_send_ptr += 3;
		*(json_send_ptr++) = '.' ;	
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;
	
	ADD_COMMA;
	add_field_name(field_name_indx++);
	ADD_DOUBLE_QUOTE;
	for(loc = 0; loc < 4; loc++){
	//	dec_ascii_byte(localm[NETIF_ETH].NetMask[loc], json_send_ptr, 3);
		json_send_ptr += 3;
		*(json_send_ptr++) = '.' ;	
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;

	ADD_COMMA;
	add_field_name(field_name_indx++);
	ADD_DOUBLE_QUOTE;
	for(loc = 0; loc < 4; loc++){
	//	dec_ascii_byte(localm[NETIF_ETH].DefGW[loc], json_send_ptr, 3);
		json_send_ptr += 3;
		*(json_send_ptr++) = '.' ;	
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;

	ADD_COMMA;
	add_field_name(field_name_indx++);
	ADD_DOUBLE_QUOTE;
	for(loc = 0; loc < 4; loc++){
	//	dec_ascii_byte(localm[NETIF_ETH].PriDNS[loc], json_send_ptr, 3);
		json_send_ptr += 3;
		*(json_send_ptr++) = '.' ;	
	}
	json_send_ptr--;
	ADD_DOUBLE_QUOTE;
	ADD_CLOSING_BRACE;

}

uint8_t  set_nw_settings(struct json_struct *wms_payload_info, uint8_t num_bytes){
	uint8_t loc, loc1, temp = 0 , temp1 = 0, temp_arr[20];
		
	if(wms_payload_info->data[1] == '1'){		// dhcp enable
		loc	= 1;    /* dhcp enable */
	//	dflash_write_multiple_byte(DHCP_INFO_ADDR, &loc, 1);
		system_info.dhcp_flag = 1;
	//	str_copy (lhost_name, (U8 *)dev_name);	
	//	init_TcpNet ();
	//	dhcp_init ();
	//	tcpsocket_init();
  	}
	else if(wms_payload_info->data[1] == '0'){		// disable the dhcp
		loc	= 0;    /* dhcp enable */
	//	dflash_write_multiple_byte(DHCP_INFO_ADDR, &loc, 1);
		system_info.dhcp_flag = 0;
		temp = 4;
		for(loc1 = 0; loc1 < 4; loc1++, temp++){
			for(loc = 0; loc < 4; loc++,temp1++){
				// system_info.nw_setting[temp1] = ascii_decimal(&wms_payload_info->data[temp], 3);	// fetch ip address
			 	temp_arr[temp1] = ascii_decimal(&wms_payload_info->data[temp], 3);	// fetch ip address
				temp += 4;
			}
//			system_info.nw_setting[temp1] = ascii_decimal(&wms_payload_info->data[temp], 3);	// fetch ip address
//			temp += 3;
		}
	//	dflash_write_multiple_byte(NW_INFO_ADDR, temp_arr, 16);
	}
//	
	update_nw_settings = 1;
	return '0';
	
}
void create_pump_status_packet(){
	create_GUI_tx_packet(GET_PUMP_STATUS,(struct tank_info *)&OHT_tank[0], 0);
}

void create_notification_status_packet(uint8_t *buff){
	create_GUI_tx_packet(SEND_NOTIFICATION, &OHT_tank[0], buff);
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/


