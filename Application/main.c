/************************************************************************//**
* @file			main.c
*
* @brief		This file contains functions required for Boot Loader.
*               
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/11/2011 \n by \em Seema Sharma
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			Separate functions can be used for accessing word or dword 
* 				as access to dword is fast as compared to a word.            			
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/

#include "stm32f0xx.h"
#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "i2c.h"
#include "eeprom.h"
#include "led.h"
#include "pin_config.h"
#include "watchdog.h"
#include "stm32f0xx_rcc.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "profile.h"
#include "rtc.h"
#include "water_management.h"
#include "buzzer.h"
#include "memory_map.h"
#include "json_client.h"
#include "client_registration.h"
#include "buyers_n_client_reg.h"
#include "stm32f0xx_syscfg.h"
#include "schedule.h"
#include "string.h"

/* Includes ------------------------------------------------------------------*/
#include "usb_bsp.h"
#include "usbd_cdc_vcp.h"



/*
**===========================================================================
**		Defines section
**===========================================================================
*/

volatile uint8_t	packet_proces_flag;    		

uint8_t 	FILE_COMPLETED_FLAG	;	

uint8_t 	APP_FLAG;				

uint8_t		INV_ADD_FLAG;			

uint8_t		NXT_PKT_FLAG;			

uint8_t		DATA_PKT_END_FLAG;		

uint8_t		CHKSUM_NOT_MATCHED;		

uint8_t		JUMP_TO_APP;

uint8_t application_exist = 1;

/*
**===========================================================================
**		Global Variable Declaration Section
**===========================================================================
*/

struct boot bl;
struct boot *bl_ptr;

uint8_t test_data = 0;

uint8_t base_address[4];

uint16_t prev_page_no = 0;

uint16_t bl_byte_cnt = 0;
uint16_t bl_data_bytes = 0;
uint8_t remaining_bl_data_bytes;

uint32_t volatile start;

uint32_t flash_write_addr;											//Application address where Firmware has to be upgrade
#define APPLICATION_ADDRESS    0x08006000      //Jump address

volatile uint16_t timeout_app = 0;
volatile uint16_t repeated_request_timeout = 0;

uint8_t data_buf[1500];
uint32_t write_data_buf[400];

uint8_t	currupt_seq_num[8] = {'~','F','F','F','F','F','F','^'};  //used for jump to app
uint8_t	app_seq_num[8] = {'~','0','0','0','0','0','0','^'};  //used for jump to app
uint8_t seq_num[6] = {'0','0','0','0','0','1'};   // send all request sequence for incoming packeket

uint8_t remaining_data;
uint8_t app_int_vec_data = 0;
uint8_t app_int_vec_addr = 0;
uint8_t led_flag = 0;


volatile uint8_t restore_flag = 0;

volatile uint8_t vsense_result, StopMode_Measure_flag;

extern struct str uart;
extern uint8_t Low_Power_Mode_Check();

extern volatile uint8_t Indication_bootloader_cnt, Indication_bootloader_flag;

/*********************************************************************************************************/



void USB_BSP_DisableInterrupt(USB_CORE_HANDLE *pdev)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Enable the USB interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USB_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = USB_IT_PRIO;
  NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
  NVIC_Init(&NVIC_InitStructure);
}


void target_deinit (void) {
	
	NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Disable the USB interrupt */
		NVIC_InitStructure.NVIC_IRQChannel = USB_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = USB_IT_PRIO;
		NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
		NVIC_Init(&NVIC_InitStructure);
	
	//	USART_DeInit (USART2);
	//	I2C_DeInit (EE_I2C);
	//	TIM_DeInit (TIM16);
	//	TIM_DeInit (TIM14);
}


void jump_to_app (void) {
		target_deinit ();
		__set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);						/* Update Main Stack Pointer. */
		(*((void(*)(void))*(__IO uint32_t*)(APPLICATION_ADDRESS + 4)))();		/* Jump to app -> Reset vector location*/
}

/************************************************************************//**
*				void delay(uint16_t del)
*
* @brief		This routine is used only to provide delay.
*				
* @param del	Delay as required
*
* @returns		None.
*
* @exception	None.
*
* @author		Seema Sharma
* @date			22/11/2011
* @note			Module Usage details and other notes.							
****************************************************************************/
void delay(uint16_t del){
	volatile uint16_t i, j;
	for(i = del; i > 0; i--)
		for(j = 50; j > 0; j--);
}


/************************************************************************//**
*					uint16_t get_rxgetptr_data(void)
*
* @brief			To get the data bye from the receive buffer if valid.
* 					
*
* @param 			None.
*
* @returns			Returns data byte if valid & negative no. if not valid.
*
* @exception	  	None.
*
* @author			Aman Deep
* @date				07/06/11
* @note			  	Module Usage details and other notes.							
****************************************************************************/
uint16_t get_rxgetptr_data(void){
	int16_t i = 0;
	if(uart.uart_rx_get_ptr != uart.uart_rx_put_ptr){						// Check getptr & putptr are not same for valid byte
		if(uart.uart_rx_get_ptr == (uart.uart_rx_buf + BUFSIZE)){
			uart.uart_rx_get_ptr = uart.uart_rx_buf;
		}			
		i  = NEXT_GETPTR_BYTE(uart.uart_rx_get_ptr);						// Get data byte from current getptr address
		i &= 0x00FF;
		uart.uart_rx_get_ptr++;
	}
	else if(uart.uart_rx_get_ptr == uart.uart_rx_buf){						// Check getptr & putptr are same & negative value returned
		if(uart.uart_rx_get_ptr  == (uart.uart_rx_buf + BUFSIZE)){
			uart.uart_rx_get_ptr =  uart.uart_rx_buf;
		}		
		i  = -1;
		i &= 0xFFFF;
	}
	return i;
}



/************************************************************************//**
*					uint16_t get_rxgetptr_data(void)
*
* @brief			Write to STM32 Flash memory
* 					
*
* @param 			None.
*
* @returns			Returns data byte if valid & negative no. if not valid.
*
* @exception	  	None.
*
* @author			Atul Kumar
* @date				07/06/11
* @note			  	Module Usage details and other notes.							
****************************************************************************/
void write_to_stm32_flash(uint32_t mem_add, uint32_t * buff, uint16_t len){
		uint16_t idx, curr_page_no;
		uint32_t clear_address;
	
		curr_page_no = (mem_add / 2048);  // uController has 1 page = 2Kbyte.
		
			/* Unlock the Flash to enable the flash control register access *************/ 
			FLASH_Unlock();
			
			/* Clear pending flags (if any) */  
			FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
	

			/* Erase the user Flash area
			(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
			if (curr_page_no != prev_page_no) {
		
				clear_address = BOOTCODE_ADD + (curr_page_no * 2048);
				/* Erasing current page. */
				FLASH_ErasePage (BOOTCODE_ADD + (curr_page_no * 2048));  //Page start address
				prev_page_no = curr_page_no;
			}

			/* Write to flash *************/ 
			for (idx = 0; idx < len; idx += 4) {

				FLASH_ProgramWord((mem_add + idx), *buff);
				buff++;
			}
			/* Lock the Flash to disable the flash control register access (recommended
			to protect the FLASH memory against possible unwanted operation) *********/
			FLASH_Lock();
}



/************************************************************************//**
*					uint16_t get_rxgetptr_data(void)
*
* @brief			Write to STM32 Flash memory
* 					
*
* @param 			None.
*
* @returns			Returns data byte if valid & negative no. if not valid.
*
* @exception	  	None.
*
* @author			Atul Kumar
* @date				07/06/11
* @note			  	Module Usage details and other notes.							
****************************************************************************/
//void write_to_stm32_flash(uint32_t mem_add, uint32_t * buff, uint16_t len){
//		uint16_t idx, curr_page_no;
//	
//		curr_page_no = (mem_add / 2048);  // uController has 1 page = 2Kbyte.
//		
//			/* Unlock the Flash to enable the flash control register access *************/ 
//			FLASH_Unlock();
//			
//			/* Clear pending flags (if any) */  
//			FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR); 
//	
//			/* Erase the user Flash area
//			(area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/
//			if (curr_page_no != prev_page_no) {
//		
//				/* Erasing current page. */
//				FLASH_ErasePage (BOOTCODE_ADD + (curr_page_no * 2048));  //Page start address
//				//FLASH_ErasePage(mem_add);
//				prev_page_no = curr_page_no;
//			}

//			/* Write to flash *************/ 
//			for (idx = 0; idx < len; idx += 4) {

//				FLASH_ProgramWord((mem_add + idx), *buff);
//			}
//			/* Lock the Flash to disable the flash control register access (recommended
//			to protect the FLASH memory against possible unwanted operation) *********/
//			FLASH_Lock();
//}



/************************************************************************//**
*					uint16_t bl_rx_data_byte(void)
*
* @brief			To get the data bye from the receive buffer if valid.
* 					
*
* @param 			None.
*
* @returns			Returns data byte if valid 
* 					Returns 0 if Packet ends or file ends
* 					Returns 0xFFFF if no more byte is available in buffer.
*
* @exception	  	None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			  	Module Usage details and other notes.							
****************************************************************************/
uint16_t bl_rx_data_byte(void){
	uint8_t data_byte = 0, j = 0;
	uint16_t byte = 0, byte1 = 0;
	
	for( j = 0; j < 2; j++ ){
		byte = get_rxgetptr_data();
		data_byte = byte;
		if(BL_HEX_PKT_START(data_byte) == TRUE){
			NXT_PKT_FLAG = 1;	
			return(0);
		}	
		else if(DATA_PKT_END(data_byte) ==  TRUE){
			DATA_PKT_END_FLAG = 1;
			return(0);
		} 
		else if(BL_FILE_END(data_byte) ==  TRUE){
			FILE_COMPLETED_FLAG = 1;
			DATA_PKT_END_FLAG = 1;
			return(0);
		}
		bl_byte_cnt++;
		if(uart.uart_rx_get_ptr == uart.uart_rx_put_ptr){
			uart.uart_rx_get_ptr -=  bl_byte_cnt;	
			return(0xFFFF);
		}
		else if(uart.uart_rx_get_ptr == uart.uart_rx_buf + BUFSIZE){
			uart.uart_rx_get_ptr = uart.uart_rx_buf;
		}
		if(j == 0){
	//		byte1 = RX_BYTE(data_byte);
			if(data_byte >= 65 && data_byte <= 90)
				byte1 = data_byte - 55;
			else if(data_byte >= 97 && data_byte <= 122){
				byte1 = data_byte - 87;
			}
			else
				byte1 = data_byte - 0x30;
			byte1 <<= 4;
		}
		else if( j == 1){
			if(data_byte >= 65 && data_byte <= 90)
				data_byte = data_byte - 55;
			else if(data_byte >= 97 && data_byte <= 122){
				data_byte = data_byte - 87;
			}
			else
				data_byte = data_byte - 0x30;
			byte1 |= data_byte;

		}
	}//for	
	return(byte1);
}

/************************************************************************//**
*					uint8_t retrieve_data(void)
*
* @brief			Store the Data bytes to be written on flash in a buffer.
* 					
*
* @param 			None.
*
* @returns			Returns 0 if valid 
* 					Returns 0xFF if no more byte is available in buffer.
*
* @exception	  	None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			  	Module Usage details and other notes.							
****************************************************************************/
uint8_t retrieve_data(void){
	uint16_t rx_byte = 0;
	uint8_t * ptr, cnt = 0;
	ptr = bl.data_buff;
	while(uart.uart_rx_get_ptr != uart.uart_rx_put_ptr){
		rx_byte = bl_rx_data_byte();


		if(rx_byte == 0xFFFF)		//no more byte in buffer
			return(0xFF); 
		else if((!NXT_PKT_FLAG) && (!DATA_PKT_END_FLAG) && (!FILE_COMPLETED_FLAG)){
			if(cnt < bl_ptr -> pkt_len)
				* ptr++ = rx_byte;
			else{ 
				bl_ptr -> chksum = 	rx_byte;
				break;	
			}
			cnt++;	
		}//else	

		if(uart.uart_rx_get_ptr == uart.uart_rx_put_ptr){
			uart.uart_rx_get_ptr -=  bl_byte_cnt;	
			return(0xFF);
		}	
	}//while
	return(0);
}




/************************************************************************//**
*					uint8_t chksum_calc(uint8_t * buff_ptr, uint8_t nob)
*
* @brief			Calculate the checksum of the data bytes received.			
*
* @param buff_ptr  	Pointer to buffer that contains data bytes.
*
* @returns			Chksum calculated.
*
* @exception		None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note				Module Usage details and other notes.
****************************************************************************/
uint8_t chksum_calc(uint8_t * buff_ptr, uint8_t nob){
	uint8_t chksum = 0, chk_inv = 0, i = 0;
	volatile uint16_t addr = 0;
	volatile uint8_t buf[2];
	chksum += bl_ptr -> pkt_len;
	addr =  bl_ptr -> address;
	buf[0] = addr;
	addr = addr >> 8;
	buf[1] = addr;
	chksum += buf[0];
	chksum += buf[1];
	chksum += bl_ptr -> rec_type;
	for(i = 0; i < nob; i++){
		chksum += * buff_ptr++;
	}	
	chk_inv  = ~chksum;
	chk_inv += 1;
	return(chk_inv);
}


/************************************************************************//**
*					void validate_hex_packet(void)
*
* @brief			Hex file packets received in data packet are validated.
* 					
* 					
* @param 			None.
*
* @returns			None.
*
* @exception	  	None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			  	Module Usage details and other notes.							
****************************************************************************/
void validate_hex_packet(void){
	uint8_t rx_data = 0;
	uint8_t i, cal_chksum = 0;
	uint16_t j,k;
	uint16_t  address, index = 0, byte = 0;
	static uint8_t int_st = 0; 
	static uint16_t cnt = 0;
	uint8_t record_type_temp = 0;
	static uint32_t base_address_int = 0;



	byte = get_rxgetptr_data();						
	if(BL_HEX_PKT_START(byte) != TRUE)										// Hex packet start byte
	return;
	
/*******Packet Length****************/
	byte = bl_rx_data_byte();
	if(byte == 0xFFFF)
		return;
	bl_ptr -> pkt_len = byte;
/**************************************/

/********Address**********************/
	for(i = 0; i < 2; i++){	
		byte = bl_rx_data_byte();

		if(byte == 0xFFFF)
			return;
		if(i == 0)
			bl_ptr -> address = byte;
		else if(i == 1){
			bl_ptr -> address *= 0x100;
			bl_ptr -> address += byte;
		}		
	}//for(i = 0; i < 2; i++)
	


	/*************************************/

	byte = bl_rx_data_byte();
	if(byte == 0x04)
		record_type_temp = byte;

	bl_ptr ->rec_type = byte;												// Record type
	if(bl_ptr ->rec_type == 1){
		FILE_COMPLETED_FLAG = 1;
	}
	rx_data = retrieve_data();
	if(rx_data == 0xFF)														// incomplete data
		return;
	if( record_type_temp == 0x04 ) {
		strncpy(base_address, bl_ptr->data_buff,2);
		base_address_int = *(base_address+0)<<24 | *(base_address+1)<<16;
		flash_write_addr = base_address_int + bl_ptr->address;
	}
	if(bl_ptr -> seq_no_rx == 2){
		flash_write_addr = flash_write_addr + bl_ptr->address;
	}
	
	CHKSUM_NOT_MATCHED = 0;
	cal_chksum = chksum_calc(bl.data_buff, bl_ptr -> pkt_len);
	
	if(cal_chksum != bl_ptr -> chksum){										// If chksum not matched
		CHKSUM_NOT_MATCHED = 1;
		return;
	}
	
	if(* uart.uart_rx_get_ptr == 0x5E){
		DATA_PKT_END_FLAG = 1;
		uart.uart_rx_get_ptr++;
	}


	if( bl_ptr ->rec_type == 0) {				// record type data

			for(i = 0; i < bl_ptr -> pkt_len; i++ ){
					data_buf[cnt + bl_data_bytes] = bl.data_buff[i];
					bl_data_bytes++;
			}		
				
			if(bl_data_bytes >= 512){
							
				memset(write_data_buf,' ',400);
				
				for(j=0,k=0; j<= 512 ; j += 4,k++) {
					write_data_buf[k] = data_buf[j+3]<<24 | data_buf[j+2]<<16 | data_buf[j+1]<<8 | data_buf[j];
				}

				application_exist = 0;          //Application has been Modified
				write_to_stm32_flash(flash_write_addr, write_data_buf, 512);
				
				flash_write_addr += 512;
				bl_data_bytes -= 512;
				if(bl_data_bytes > 0){
					memcpy(data_buf, (data_buf + 512), bl_data_bytes);	
				}
			
			}		
//			else{
//				for(i = 0; i < bl_ptr -> pkt_len; i++ ){
//					data_buf[cnt + bl_data_bytes] = bl.data_buff[i];
//					bl_data_bytes++;
//				}				
//			}
	
	}

}



/************************************************************************//**
*					long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine is used to convert the ascii bytes to decimal value
*
*
* @param *convrt_ptr		Pointer to the variable that contains ascii bytes.
* @param nob				No. of data bytes to be converted.
*
* @returns					Converted long decimal value.
*
* @exception				None.
*
* @author					Nikhil kukreja
* @date						06/02/13
* @note						None.
****************************************************************************/
long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob){
	char loop;
	long cnvrt_val = 0;

	for(loop = 0 ; loop < nob ; loop++){
		cnvrt_val = (cnvrt_val * 10) + (*(convrt_ptr++) - 48);
	}
	 return cnvrt_val;
}

/************************************************************************//**
*					dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob)
*
* @brief			This routine converts the decimal value to ascii bytes
*
* @param 			nob  - tells the number of bytes to be converted.
*
*					long_value - to be converted into decimal ascii bytes
*
*					*convrt_ptr - pointer to buffer that contains data bytes to be converted
*
* @returns			None.
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				08/02/13
* @note				None.
****************************************************************************/
void dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob){

	while(nob > 0){
		*(convrt_ptr + nob - 1) = (long_value % 10) + 0x30;
		long_value = long_value/ 10;
		nob--;
	}
}


/************************************************************************//**
*					void bl_retrieve_data_bytes(void)
*
* @brief			Received packet is validated and written to flash and 
* 					acknowledgement(next seq no.)is transmitted.
* 					
* @param 			None.
*
* @returns			None.
*
* @exception	  	None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			  	Module Usage details and other notes.							
****************************************************************************/
void bl_validate_data_packet(void){
	uint8_t i, loc_count; //seq[2];
	uint8_t atul_buff[60];
	uint8_t firmware_upgrade = 0;	
	uint16_t j,k,byte = 0, loc_val;
	
	bl_byte_cnt = 0;

	
	
	// check for start byte
	// extract sequence number
	// extract data bytes
	// fill the data bytes to flash write array till stop bit or end of file not received
	// send an ack packet

	while(uart.uart_rx_put_ptr != uart.uart_rx_get_ptr){
	byte = get_rxgetptr_data();
		
	if(byte == '~'){		// start sequence found
		
		for(loc_count = 0; loc_count < 6; loc_count++){
			seq_num[loc_count] = get_rxgetptr_data();
		}
		bl_ptr->seq_no_rx = ascii_decimal(seq_num, 6);	// extract sequence number
		
		
		
		// compare with previous received chunk number if wrong
		if(bl_ptr -> seq_no_rx != bl_ptr -> seq_no){
			dec_ascii_byte(bl_ptr -> seq_no, seq_num, 6);
			uart.uart_rx_put_ptr = uart.uart_rx_buf;
			uart.uart_rx_get_ptr = uart.uart_rx_put_ptr;

			usb_send_data(seq_num, 6);
			
			#ifdef E4
  		uart_send_str(USART2, seq_num, 6);
			#endif
			
			bl_byte_cnt= 0;
			DATA_PKT_END_FLAG = 0;
			return;
		}
		
		// compare with previous received chunk number if right
		if(bl_ptr -> seq_no_rx == bl_ptr -> seq_no){
			NXT_PKT_FLAG = 0;
			
			while(!DATA_PKT_END_FLAG){
				validate_hex_packet();
				
				if((CHKSUM_NOT_MATCHED == 1)){
					DATA_PKT_END_FLAG = 1;
					break;
				}
				
				
				if(FILE_COMPLETED_FLAG){
					
					memset(write_data_buf,' ',400);							// write remaining bytes in memory
						
					for(j=0,k=0; j<= 512 ; j += 4,k++) {
						write_data_buf[k] = data_buf[j+3]<<24 | data_buf[j+2]<<16 | data_buf[j+1]<<8 | data_buf[j];
					}
									
					write_to_stm32_flash(flash_write_addr, write_data_buf, bl_data_bytes);
					firmware_upgrade = 0;
					eeprom_data_read_write(WMS_NEW_FIRMWARE_PRESENT_ADDRESS, WRITE_OP, &firmware_upgrade, 1);  //firmware upgraded successfully
					
						//Send Ack for jump to Application
					usb_send_data(app_seq_num, 8);
					
					#ifdef E4
  			  uart_send_str(USART2, app_seq_num, 8);
					#endif
					
					for (start = 0; start < 1000000; start++) { ; }
					for (start = 0; start < 1000000; start++) { ; }
					jump_to_app();
				}
			}
		} //compare with previous received chunk number

		
		if((FILE_COMPLETED_FLAG == 0) && (DATA_PKT_END_FLAG == 1)){
			if(CHKSUM_NOT_MATCHED == 0){
				bl_ptr->seq_no_rx++;
				bl_ptr -> seq_no++;
			}
			
			dec_ascii_byte(bl_ptr->seq_no_rx, seq_num, 6);  //increment sequence number
			uart.uart_rx_put_ptr = uart.uart_rx_buf;
			uart.uart_rx_get_ptr = uart.uart_rx_put_ptr;

			usb_send_data(seq_num, 6);
			
			#ifdef E4
  		uart_send_str(USART2, seq_num, 6);
			#endif
			
			bl_byte_cnt= 0;
			DATA_PKT_END_FLAG = 0;
		}//if(!FILE_COMPLETED_FLAG)
		
	 } // start sequence found

	} //uart.uart_rx_put_ptr != uart.uart_rx_get_ptr
}




/************************************************************************//**
*				void init_target(void)
*
* @brief		Execute the code whatever should be executed(Boot or Application).			
*
* @param    	None.
*
* @returns		None.
*
* @exception	None.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			Module Usage details and other notes.
****************************************************************************/
void pre_init(void){
	volatile uint16_t temp = 0;
	uint16_t i;
	uint8_t atul_buff[512], t_buf ;

	APP_FLAG = 0;
	JUMP_TO_APP = 0;

	eeprom_data_read_write(WMS_NEW_FIRMWARE_PRESENT_ADDRESS, READ_OP, &t_buf, 1);
	if(t_buf == 1){			// If t_buf=1 => boot code is executed, else Application code is executed 
		APP_FLAG = 1;
		usb_send_data(seq_num, 6);     		//Request for New Firmware
		#ifdef E4
  	uart_send_str(USART2, seq_num, 6);
		#endif

  	bl_ptr = &bl;														
  	bl_ptr -> seq_no = 1; 										// 1st data packet sequence no.											
		INV_ADD_FLAG = 0;													// clear flags
		DATA_PKT_END_FLAG = 0;
		FILE_COMPLETED_FLAG = 0;		
	}
	else{					//Send Ack for jump to Application
//			usb_send_data(app_seq_num, 8);
//			#ifdef E4
//  		uart_send_str(USART2, app_seq_num, 8);
//			#endif
//		
//			for (start = 0; start < 1000000; start++) { ; }
//			for (start = 0; start < 1000000; start++) { ; }
			jump_to_app();
	}//else		
}

void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
	NVIC_SystemReset();
}

/************************************************************************//**
*				void main(void)
*
* @brief		Initialize clock, keypad, uart and timer
*               Reads Sensirion Temperature Sensor(SHT15) every 30 Secs
* 				Enable Global Interrupt and other Low Power Mode Interrupts.
*
* @author		Atul Kumar
* @date			06/02/2017
* @note			Module Usage details and other notes.
****************************************************************************/
void main(void){
	uint16_t temp;
	uint8_t atul_buff[60];
	uint8_t t_buf;

	
	/**********************************************************/
	/*			  Wait For Debugger Connection				  */
	/**********************************************************/
	
	//To have the boot loader work again you'd need to Remap back to FLASH starting address
	/* Remap SRAM at 0x08000000 */
	
	SYSCFG_MemoryRemapConfig(SYSCFG_MemoryRemap_Flash);
	
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
	
	if(Low_Power_Mode_Check()==0){	

		StopMode_Measure();			//call low power modes used for rtc	
		NVIC_SystemReset();
	}
	else{	
		Init_Target ();
		usbd_init();
	}
	pre_init();						//Check for firmware upgrade

	while(1){

		//while(1);
		if(StopMode_Measure_flag){	    //Stop Mode Entry
		//	vsense_result = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);
			if(Low_Power_Mode_Check()==0) { 
				StopMode_Measure();			//call low power modes used for rtc
				NVIC_SystemReset();
			}
			StopMode_Measure_flag = 0;
		}
		
			
		if(Indication_bootloader_flag) {   //Bootloader running indication
			Indication_bootloader_flag = 0;
			GPIO_ToggleBits(GPIOB, GPIO_Pin_0);
		}
		
		if ( (timeout_app >= APP_WAIT_TIME) && (application_exist == 1) ){  //if no packet received till 2 Min and application has not modified 
			APP_FLAG = 0; 
			timeout_app = 0;

			usb_send_data(currupt_seq_num, 8);  	//Send Ack for jump to Application
			#ifdef E4
  		uart_send_str(USART2, currupt_seq_num, 8);
			#endif
			for (start = 0; start < 1000000; start++) { ; }
			for (start = 0; start < 1000000; start++) { ; }
			jump_to_app();
		}
		
		if((repeated_request_timeout >= REPEATED_REQUEST_TIME_OUT) && (FILE_COMPLETED_FLAG == 0) ){
			usb_send_data(seq_num, 6);
			#ifdef E4
  		uart_send_str(USART2, seq_num, 6);
			#endif
			repeated_request_timeout = 0;
		}

		if(packet_proces_flag){              //If packet received, Validate and process it
			APP_FLAG = 0;
			packet_proces_flag = 0;
			bl_validate_data_packet();
		}
  
	}//while(1)


}//main


