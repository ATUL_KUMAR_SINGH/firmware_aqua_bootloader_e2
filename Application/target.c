/********************************************************************************************************************************
 * File name:	 	target.c
 *
 * Attention:		Copyright 2015 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 22/12/2015 by Sonam
 *
 * Description: 	This module is used to initialize all peripheral on Cool Smart.
 *******************************************************************************************************************************/


/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "stm32f0xx.h"
#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "i2c.h"
#include "eeprom.h"
#include "led.h"
#include "pin_config.h"
#include "watchdog.h"
#include "stm32f0xx_rcc.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "profile.h"
#include "rtc.h"
#include "water_management.h"
#include "buzzer.h"
#include "memory_map.h"
#include "json_client.h"
#include "client_registration.h"
#include "m-35_config.h"

/********************************************************************************************************************************
* Defines section
 *******************************************************************************************************************************/

/********************************************************************************************************************************
* Global declarations section
 *******************************************************************************************************************************/

uint8_t module_count;

extern uint8_t *nw_strings[2];  // Assid, Akey

extern struct wifi_init_params  wifi_param_obj;

/********************************************************************************************************************************
 * Function name: 	void init_target(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		
 *
 * Date created: 	
 *
 * Description: 	Initializes all the modules present in Cool Smart.
 *
 * Notes:
 *******************************************************************************************************************************/
void Init_Target(void){
	
  uint8_t profile_data[100] , read_buff[100], count, indx, loc, buff[20], temp_arr[50], perform_default_wr;
  uint32_t loc_var, level = 0 ;
	uint32_t volatile start;
	uint8_t t_buf;
	uint8_t len, idx;

	
	/**********************************************************/
	/*			 Initialize USB									 				  */
	/**********************************************************/
	
	
	
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	for (start = 0; start < 1000000; start++) { ; }
//	
	/**********************************************************/
	/*			 Initialize OAMP & Device LEDs 				  */
	/**********************************************************/
 
	led_Init();
 
	
	/**********************************************************/
	/*			 Initialize Relay at PA0 && PA1 				  */
	/**********************************************************/

	 RELAY_OHT_Init();


	/**********************************************************/
	/*	 Initialize Digital Inputs PB0-PB-15 && PC5-PC7 	  */
	/**********************************************************/


	init_Digitalinputs ();


	/**********************************************************/
	/*			Initialize Button on Ext Interrupt 			  */
	/**********************************************************/


	EXTILine2_Config();	// Configure External Input PUMP1Button as Input


	/**********************************************************/
	/*					   Initialize EEPROM 				  								*/
	/**********************************************************/
	
	 i2c_init ();
	 EEPROM_WP_init();
	 
	
	/**********************************************************/
	/*					   Initialize Timers  				  							*/
	/**********************************************************/


	timer_init(TIM16,TIME_MILLI_SEC, 10);     //Led Display control
  TIM_Cmd (TIM16, ENABLE);

	timer_init(TIM14,TIME_MILLI_SEC, 1000);    //Buzzer Control
	TIM_Cmd (TIM14, ENABLE);
	


  /**********************************************************/
	/*					   Initialize UARTS 				  */
	/**********************************************************/

	uart_init (USART2, 115200);	

	/**********************************************************/
	/*			Initialize ADC for Battery & Version 		  */
	/**********************************************************/
	
	
	#ifdef E4
	eeprom_data_read_write(WMS_NEW_FIRMWARE_PRESENT_ADDRESS, READ_OP, &t_buf, 1);
	if(t_buf == 1){			// If t_buf=1 => boot code is executed, else Application code is executed 
		
	at_command_mode();
		
 module_count = 3;

  GPIO_ResetBits(GPIOB, GPIO_Pin_7);
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
	if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{
		GPIO_SetBits(GPIOB, GPIO_Pin_7);
	}

	

	module_count = 5;
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
  if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{

	}
	
	module_count = 4;
	GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
  	if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{
		  GPIO_SetBits(GPIOB, GPIO_Pin_6);
	}

	
	module_count = 3;
	while((module_count != 0) && (Low_Power_Mode_Check() == 1));
	  	if(Low_Power_Mode_Check() == 0)
	{
		return;
	}
	else
	{

	} 
	
	
	
	eeprom_data_read_write(M35_MODE_ADDR, READ_OP, &wifi_param_obj.mode, 1);	// mode = AP/STA
	
	if(wifi_param_obj.mode == M35_MODE_AP)
	{
		strcpy(wifi_param_obj.nw_Assid, nw_strings[0]);
		strcpy(wifi_param_obj.nw_AKey, nw_strings[1]);
	}
	else
	{
		len = 0;
		idx = 0;
		memset(&temp_arr[0], 0x00,sizeof(temp_arr));
		eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, READ_OP, &temp_arr[0], M35_SSID_MAXLEN);	
		while(temp_arr[idx] != '#')
		{
			len++;
			if(len > M35_SSID_MAXLEN){
				break;
			}
			idx++;
		}

		strncpy(&wifi_param_obj.nw_Sssid[0], &temp_arr[0], len);
		
		len = 0;
		idx = 0;
		memset(&temp_arr[0], 0x00,sizeof(temp_arr));
	  	eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, READ_OP, &temp_arr[0], M35_KEY_MAXLEN);
	
		while(temp_arr[idx] != '#')
		{
			len++;
			idx++;
		}

		strncpy(&wifi_param_obj.nw_SKey[0], &temp_arr[0], len);
	}
	
//	m_35_module_config();
	//m_35_module_params_config(&json_info, CONFIG_WITHOUT_PKT);
	m_35_module_params_config(0, CONFIG_WITHOUT_PKT);
	m_35_module_params_config_cmds_send(&wifi_param_obj);
	
	//m_35_module_config();
	
  //wifi_enable();
	}
	#endif
	
	/**********************************************************/
	/*			Initialize RTC                      		  */
	/**********************************************************/

	RTC_initialize();
  //write_RTC(time, 2017);


}
