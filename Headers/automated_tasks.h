/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef AUTOMATED_TASKS_H
#define AUTOMATED_TASKS_H

#include "stm32f0xx.h"
//#include "ethernet_packet.h"
#include"json_client.h"

/*
**===========================================================================
**		Defines section
**===========================================================================
*/
#define WRITE_DEFAULT_BOTH_TANKS				0

#define AUTO_SETTINGS_UGT_TANK_DISABLE			1

#define AUTO_SETTINGS_UGT_PUMP_DISABLE			2

#define	AUTO_SETTINGS_UGT_TANK_ENABLE				3

#define	AUTO_SETTINGS_UGT_PUMP_ENABLE				4

#define	NOT_DEFINE						2

struct auto_task_state{
	uint8_t oht_pump: 2;
	uint8_t ugt_pump: 2;
};


uint8_t auto_tsk_process(void);
void get_automated_task_status(uint8_t *param);
//void get_automated_task_status(struct json_struct *wms_payload_info, uint8_t num_bytes);
//void get_automated_task_status(void);
void check_automated_task(uint8_t tank_num);
void write_default_automated_task(void);
void automated_task_enable(struct json_struct *wms_payload_info, uint8_t num_bytes);
void get_automated_task_status_create_packet(void);
uint8_t automated_task_process(void);
void enable_automated_task(uint16_t task_enable, uint16_t state);
void create_automated_tsk_response(struct json_struct *wms_payload_info, uint8_t num_bytes);

#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
