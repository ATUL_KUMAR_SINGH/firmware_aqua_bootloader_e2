/************************************************************************//**
* @file			"json_client.h"
*
* @brief		Header for json_client.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 10/12/11 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef JSON_CLIENT_H
#define JSON_CLIENT_H

#include "water_management.h"
#include <stm32f0xx.h>

#pragma pack(1)
struct json_struct{
	uint8_t 	socket_num;
	uint16_t 	server_id;
	uint8_t 	slave_addr;
	uint16_t 	cmd;
	uint8_t		idx;
	uint8_t		lmt;
	uint8_t 	data[800];		

};


#define	JSON_SERVER_ID					9000

#define BUYERS_REGISTRATION			8228

#define	GUI_WMS_DEVICE_REGISTRATION				8200

#define	GUI_WMS_SAVE_DATE_TIME					8102

#define	GUI_WMS_GET_DATE_TIME					8103


#define	GUI_WMS_AUTHENTICATE_MAC_ID				8208

#define	GUI_WMS_VIEW_DEVICE_REGISTRATION_LIST		8210

#define	GUI_WMS_UPDATE_DEVICE_REGISTRATION_LIST		8209

#define	GUI_WMS_DEVICE_REGISTRATION_REMOVE		8211

#define	SET_TANK_PUMP_SENSOR_SETTINGS		    8501

#define	GET_TANK_PUMP_SENSOR_SETTINGS		    8502

#define	PUMP_ON								    2000

#define	PUMP_OFF							    2001

#define	GET_PUMP_STATUS						    2003

#define	SAVE_WATER_USAGE_SETTING				8516

#define	GET_WATER_USAGE_SETTING					8517

#define	SEND_NOTIFICATION						8510

#define	GET_NOTIFICATION						8511

#define	SEND_CURRENT_WATER_LEVEL				8518

#define	GET_HOURLY_CONSUMPTION					8505

#define	GET_HOURLY_AVAILABILITY					8508

#define	WMS_GUI_SEND_HOURLY_CONSUMPTION			8504

#define	WMS_GUI_GET_WATER_CONSUMPTION			8506

#define	WMS_GUI_SEND_HOURLY_AVAILABILITY		8507

#define	GET_EVENT_LOG						    8523

#define	SEND_EVENT_LOG						    8524

#define	GET_CURRENT_WATER_LEVEL_PUMP_STATUS	    8519

#define	GET_CURRENT_WATER_LEVEL				    8520

#define	SEND_LICENSE_NUM					    8215

#define	GUI_WMS_SAVE_TANK_SCHEDULE				7004

#define	GUI_WMS_GET_TANK_SCHEDULE				7009

#define	SET_NW_SETTING							8005

#define	GET_NW_SETTING							8008

#define	GUI_ENABLE_BROADCAST					8010

#define	GUI_WMS_DEVICE_PASSWORD_AUTHENTICATE	8212

#define	GUI_WMS_DEVICE_PASSWORD_CHANGE			8204

#define	GUI_WMS_DEVICE_PASSWORD_FORGET			8213

#define	GUI_WMS_SET_BUZZER_STATE				8512

#define	GUI_WMS_GET_BUZZER_STATE				8515

#define	FIRMWARE_UPGRADE						8003

#define	GET_FW_VERSION_NUMBER					8004

#define	GET_HW_VERSION_NUMBER					8040

#define	BROADCAST_IP_ADDRSS						8009

#define	GUI_WMS_ADD_LICENSE						8214   

#define	GUI_WMS_GET_LICENSE_NUM					8215

#define	GUI_WMS_ADD_SERIAL_NUM					8216   

#define	GUI_WMS_GET_SERIAL_NUM					8217

#define	GUI_WMS_ADD_MAC_ID						8218   

#define	GUI_WMS_GET_MAC_ID						8219

#define	GET_SERVER_LICENSE_STATUS				8225



#define	WMS_GUI_AC_MAINS_STATUS					1


#define	ADD_CLOSING_BRACE		*(json_send_ptr++) = '}';

#define	ADD_NULL_CHAR			*(json_send_ptr++) = '\0';

#define	ADD_START_BRACE			*(json_send_ptr++) = '{';

#define	ADD_COMMA				*(json_send_ptr++) = ',';

#define	ADD_DOUBLE_QUOTE		*(json_send_ptr++) = '"';

#define	PACKET_VALIDATED_RESPONSE_BROADCAST			1

#define	PACKET_VALIDATED_RESONSE_HTTP				3

#define	PACKET_VALIDATED							2

#define	JSON_INVALID								4



#define START							0

#define SERVER_ID_EXTRACTED				1

#define SAD_EXTRACTED					2

#define CMD_EXTRACTED					3

#define FIELD_NAME_EXTRACTED			4

#define FIELD_VAL_EXTRACTED				5

#define INDEX_EXTRACTED					6

#define LIMIT_EXTRACTED					7

//#define	MAX_FIELD_LEN					 30
#define	MAX_FIELD_LEN					 60

#define	NO_DATA_FOUND					18

#define	SCHEDULE_SAVE_ERROR				19

#define	SCHEDULE_DISABLE_ERROR			20

#define	SCHEDULE_DELETE_ERROR			21


/*

#define START							0

#define EXTRACT_COMMAND_NAME			1

#define EXTRACT_COMMAND_VAL				2

#define EXTRACT_DEVICE_NAME				3

#define EXTRACT_DEVICE_VAL				4

#define EXTRACT_FIELD_NAME				5

#define EXTRACT_FIELD_VAL				6

*/

/*
#define START							0

#define GET_START_EXTRACT_FIELD_NAME	1

#define GET_START_EXTRACT_FIELD_VAL		2

#define EXTRACT_FIELD_NAME				3

#define EXTRACT_FIELD_VAL				4


*/

#define	SERVER_ID_NOT_FOUND				3

#define	EXPRESSION_INCOMPLETE			2

#define	EXPRESSION_VALIDATE				1

enum command_list {val1 = GUI_WMS_UPDATE_DEVICE_REGISTRATION_LIST};

uint8_t copy_json_field_val(uint8_t *json_payload);
uint8_t extract_json_field_val(uint8_t *ptr, uint8_t *json_payload);
void add_field_name(uint8_t field_num);
uint8_t dec_ascii_arr(uint32_t long_value, uint8_t *convrt_ptr);
void add_json_header(uint32_t command);
void create_GUI_tx_packet(uint32_t command, struct tank_info *tank_ptr, uint8_t *param);
void create_copy_response(struct json_struct *wms_payload_info, uint16_t num_bytes);
//void get_tank_status(struct tank_info *tank_ptr);
void intit_JSON_send_header(void);
void add_field_val(uint8_t *ptr, uint8_t len);
long ascii_decimal(uint8_t *convrt_ptr, uint8_t nob);
void dec_ascii_byte(uint32_t long_value, uint8_t *convrt_ptr, uint8_t nob);
uint16_t JSON_GUI_packet_process(struct json_struct *wms_payload, uint16_t total_byte_packet);

void create_notification_status_packet(uint8_t *buff);
void notification_info_oht(void) ;

#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/



