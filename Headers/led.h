#ifndef __LED_H__
#define __LED_H__

#include "stm32f0xx_gpio.h"



		/**********************************************************/
		/*						Tank1				 			  */
		/**********************************************************/

#define 	LED_PORT         		  GPIOA

#define 	OHT_LED_RED		       	GPIO_Pin_5
#define 	OHT_LED_GREEN		     	GPIO_Pin_6

#define 	TURN_OHT_GREEN				 GPIO_SetBits(LED_PORT, OHT_LED_GREEN);GPIO_ResetBits(LED_PORT, OHT_LED_RED)
#define 	TURN_OHT_RED				   GPIO_ResetBits(LED_PORT, OHT_LED_GREEN);GPIO_SetBits(LED_PORT, OHT_LED_RED)
#define 	TURN_OHT_ORANGE				 GPIO_SetBits(LED_PORT, OHT_LED_GREEN);GPIO_SetBits(LED_PORT, OHT_LED_RED)
#define 	TURN_OHT_OFF				   GPIO_ResetBits(LED_PORT, OHT_LED_GREEN);GPIO_ResetBits(LED_PORT, OHT_LED_RED)


		/**********************************************************/
		/*						Tank2				 			  */
		/**********************************************************/

#define 	UGT_LED_RED			         GPIO_Pin_7
#define 	UGT_LED_GREEN			       GPIO_Pin_0



#define 	TURN_UGT_GREEN			     GPIO_ResetBits(GPIOA, UGT_LED_RED);GPIO_SetBits(GPIOB, UGT_LED_GREEN)
#define 	TURN_UGT_RED			       GPIO_SetBits(GPIOA, UGT_LED_RED);GPIO_ResetBits(GPIOB, UGT_LED_GREEN)
#define 	TURN_UGT_ORANGE			     GPIO_SetBits(GPIOA, UGT_LED_RED);GPIO_SetBits(GPIOB, UGT_LED_GREEN)
#define 	TURN_UGT_OFF				     GPIO_ResetBits(GPIOA, UGT_LED_RED);GPIO_ResetBits(GPIOB, UGT_LED_GREEN)


		/**********************************************************/
		/*						LED_OAMP				 			  */
		/**********************************************************/

#define 	OAMP_LED_RED			           	GPIO_Pin_4
#define 	TURN_OAMP_ON				GPIO_SetBits(GPIOA, OAMP_LED_RED);
#define 	TURN_OAMP_OFF				GPIO_ResetBits(GPIOA, OAMP_LED_RED);



		/**********************************************************/
		/*						LED_RF				 			  */
		/**********************************************************/

#define 	LED_RF_PORT	                    GPIOD
#define 	LED_RF_PIN			           	GPIO_Pin_2
#define 	LED_RF_ENABLE				GPIO_SetBits(LED_RF_PORT, LED_RF_PIN)
#define 	LED_RF_DISABLE				GPIO_ResetBits(LED_RF_PORT, LED_RF_PIN)




		/**********************************************************/
		/*						BUZZER_OUT				 		  */
		/**********************************************************/

#define 	System_Led2			           	GPIO_Pin_4
#define 	System_Led2_ENABLE				GPIO_SetBits(LED_PORT, System_Led2)
#define 	System_Led2_DISABLE				GPIO_ResetBits(LED_PORT, System_Led2)


		/**********************************************************/
		/*						EEPROM write protection       				 		  */
		/**********************************************************/

#define 	EEPROM_WRITE_ACCESS_ENABLE		GPIO_ResetBits(GPIOA, GPIO_Pin_5)
#define 	EEPROM_WRITE_ACCESS_DISABLE		GPIO_SetBits(GPIOA, GPIO_Pin_5)


//		/**********************************************************/
//		/*						RELAY_PUMP_OHT				 	  */
//		/**********************************************************/
//
#define 	RELAY_OHT_PORT          		GPIOB
#define 	RELAY_OHT_PIN          			GPIO_Pin_10
#define 	RELAY_OHT_ENABLE				GPIO_SetBits(RELAY_OHT_PORT, RELAY_OHT_PIN)
//#define 	RELAY_OHT_DISABLE				GPIO_ResetBits(RELAY_OHT_PORT, RELAY_OHT_PIN)
//
//
//		/**********************************************************/
//		/*						RELAY_PUMP_UGT				 	  */
//		/**********************************************************/
//
//#define 	RELAY_UGT_PORT          		GPIOA
//#define 	RELAY_UGT_PIN          			GPIO_Pin_1
//#define 	RELAY_UGT_ENABLE				GPIO_SetBits(RELAY_UGT_PORT, RELAY_UGT_PIN)
//#define 	RELAY_UGT_DISABLE				GPIO_ResetBits(RELAY_UGT_PORT, RELAY_UGT_PIN)

#define LED_PULSE		10
#define LED_SHORT		20
#define LED_LONG		30
#define LED_CYCADJ		40
#define LED_PULSE_INTERVAL	50


#define LED_OFF			1
#define LED_RED			2
#define LED_GREEN		3
#define LED_ORANGE		4


#define INTPULSE_OFF	(LED_PULSE_INTERVAL + LED_OFF)

#define PULSE_OFF		(LED_PULSE + LED_OFF)
#define PULSE_RED		(LED_PULSE + LED_RED)
#define PULSE_GREEN		(LED_PULSE + LED_GREEN)
#define PULSE_ORANGE	(LED_PULSE + LED_ORANGE)

#define SHORT_OFF		(LED_SHORT + LED_OFF)
#define SHORT_RED		(LED_SHORT + LED_RED)
#define SHORT_GREEN		(LED_SHORT + LED_GREEN)
#define SHORT_ORANGE	(LED_SHORT + LED_ORANGE)

#define LONG_OFF		(LED_LONG + LED_OFF)
#define LONG_RED		(LED_LONG + LED_RED)
#define LONG_GREEN		(LED_LONG + LED_GREEN)
#define LONG_ORANGE		(LED_LONG + LED_ORANGE)

#define CYCADJ_OFF		(LED_CYCADJ + LED_OFF)
#define CYCADJ_RED		(LED_CYCADJ + LED_RED)
#define CYCADJ_GREEN	(LED_CYCADJ + LED_GREEN)
#define CYCADJ_ORANGE	(LED_CYCADJ + LED_ORANGE)


#define OHT_TANK		0
#define UGT_TANK		1

#define OAMP			1




void LED_OAMP(void);

#endif



