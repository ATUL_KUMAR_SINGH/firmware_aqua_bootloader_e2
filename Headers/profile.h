/************************************************************************//**
* @file			profile.h
*
* @brief		This module contains the profile macros & definiations.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/
#ifndef __PROFILE_H__
#define __PROFILE_H__
/*
**===========================================================================
**		Include section
**===========================================================================
*/		   
#include "stm32f0xx.h"
#include "std_periph_headers.h"
#include "led.h"


	 /**********************************************************/
	/*					   Device Profile Struct			  */
	/**********************************************************/
typedef struct { 	 
	uint8_t profile_state;
	uint8_t profile_no;
	uint8_t ac_operation_via;
	uint8_t temperature_manual_timer;
	uint8_t pattern;
	uint8_t fan_speed;
	uint8_t swing;
	uint8_t ac_mode;
	uint8_t ac_timer;
	uint8_t vent;
} device_profile;


#endif
