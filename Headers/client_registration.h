/************************************************************************//**
* @file			client_registration.h
*
* @brief		Header for protocol_conv.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 1/02/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef CLIENT_REGISTRATION_H
#define CLIENT_REGISTRATION_H

#include "stm32f0xx.h"
//#include "ethernet_packet.h"
#include"json_client.h"
#include "memory_map.h"
/*
**===========================================================================
**		Defines section
**===========================================================================
*/

#define		SYSTEM_DEVICE_REGISTRATION					'1'

#define		SYSTEM_DEVICE_REGISTRATION_SEND_SETTING		'2'

#define		DEVICE_REGISTRATION							'3'

//#define		WEB_REGISTRATION							'3'

///////////////////////////////////////////////
//DEVICE REGISTRATION

#define	TOTAL_REG_KEY					3


#define	FW_VER_GREATER_THAN_3_7_25           //For 10 Schedule	
	
#ifdef FW_VER_GREATER_THAN_3_7_25
	#define	APP_ID_LEN						16
//#else
//	#define	MAC_ID_LEN						12
#endif

//#define	DEVICE_REG_LEN					19

#define	REGISTRATION_SUCESSFULL		   	0x30

#define	INVALID_REGISTRATION_KEY	   	0x31

#define	REGISTRATION_KEY_ALREADY_BOUND		0x32

#define	REGISTRATION_KEY_NOT_ACTIVE		 0x32

#define	REGISTRATION_KEY_FREE			0x30

#define	REGISTRATION_KEY_BIND			0x31

#define	REGISTRATION_KEY_INACTIVE		0x32

#define	REGISTRATION_REPLACE		   	0x30

#define	MAC_ID_NOT_FOUND				0x31

#define	INVALID_REG_KEY					0x32

#define	MAC_ID_FOUND_REPLACED			0x30

#define DEVICE_REG_START_ADDR			100			    // Memory address

#define	USER_NAME_LEN					12

void device_registration_init(void);

void clear_device_registration_init(void);

//uint8_t device_registration(struct json_struct *wms_payload_info, uint8_t num_bytes);

uint8_t authenticate_MAC_ID(struct json_struct *wms_payload_info, uint8_t num_bytes);

void fetch_device_registration_list(void);

//void fetch_device_registration_list(struct gui_payload *wms_payload_info, uint8_t num_bytes);

uint8_t remove_MAC_ID(struct json_struct *wms_payload_info, uint8_t num_bytes);

uint8_t device_replace(struct json_struct *wms_payload_info, uint8_t num_bytes);

#pragma pack(1)
struct sys_info{
	uint8_t status;
	uint8_t	serial_num[SERIAL_NUM_LEN];
	uint8_t	mac_id[12];
	uint8_t	license_num[24];
	uint8_t	total_key;
	uint8_t	dhcp_flag;
	uint8_t	nw_setting[16];
	uint8_t fw_ver[3]; 

};

struct client_info{
   	char user_info[USER_NAME_LEN + 1];
	char passwrd_info[REGISTRATION_KEY_LEN + 1];	
};

//
//struct client_info{
//	char total_reg_client;
//	char user_info[TOTAL_HTTP_SESSION][USER_NAME_LEN];
//	char passwrd_info[TOTAL_HTTP_SESSION][REGISTRATION_KEY_LEN];
//
//};

#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
