/************************************************************************//**
* @file			adc.h
*
* @brief		Header for Target.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 07/06/16 \n by \em Atul Kumar
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
****************************************************************************/
#ifndef __ADC_H 
#define __ADC_H

/*
**===========================================================================
**		Include section
**===========================================================================
*/
/*
**===========================================================================
**		MACRO Defines section
**===========================================================================
*/

#define		ADC_GAIN				7.35

#define 	RTD_SCAN_PERIOD 		3000

#define 	PUMP_UGT_ADC_CHN		ADC_Channel_10
#define 	PUMP_OHT_ADC_CHN		ADC_Channel_0
#define 	VER_CONTROL_ADC_CHN		ADC_Channel_2		
#define 	VSENSE_ADC_CHN		    ADC_Channel_1
#define 	SOLAR_ADC_CHN			ADC_Channel_14


#define		OHT_FEEDBACK_CHNL	1

#define		UGT_FEEDBACK_CHNL	2	

#define		EVT_ONE_SEC_EVNT		3

#define		EVT_OHT_FEEDBACK_CHNL	1

#define		EVT_UGT_FEEDBACK_CHNL	2

/*
**===========================================================================
**		Defines section
**===========================================================================
*/
/* Initialization and Configuration functions *********************************/
//void ADC_interrupt_config(void);
//void ADC_IRQHandler(void);
//void ADC_3_Config(void);
//void RTD_ADC_Config (void);
//void activate_adc3_channel (uint8_t channel);
//void initialize_adc3 (void);
//void enable_rtd_scan (void);
//void schedule_rtd_adc (void);
//void ReadHwVerRead (void);





void ADC_interrupt_config();
void ADC_1_Config(void);
void activate_adc1_channel (uint32_t channel);
uint16_t readADC1(uint32_t channel);
void select_feedback_chnl(void);
void ADC1_COMP_IRQHandler(void);

#endif
