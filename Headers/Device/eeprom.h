#ifndef __EEPROM_H__
#define __EPROM_H__

#define EEPROM_ADDR					0xAE
#define WRITE_OP						0
#define READ_OP							1

#define EE_I2C                          I2C1
#define EE_I2C_CLK                      RCC_APB1Periph_I2C1
   
#define EE_I2C_SCL_PIN                  GPIO_Pin_8                  /* PB.08 */
#define EE_I2C_SCL_GPIO_PORT            GPIOB                       /* GPIOB */
#define EE_I2C_SCL_GPIO_CLK             RCC_AHBPeriph_GPIOB
#define EE_I2C_SCL_SOURCE               GPIO_PinSource8
#define EE_I2C_SCL_AF                   GPIO_AF_1				//GPIO_AF_1

#define EE_I2C_SDA_PIN                  GPIO_Pin_9                  /* PB.09 */
#define EE_I2C_SDA_GPIO_PORT            GPIOB                       /* GPIOB */
#define EE_I2C_SDA_GPIO_CLK             RCC_AHBPeriph_GPIOB
#define EE_I2C_SDA_SOURCE               GPIO_PinSource9
#define EE_I2C_SDA_AF                   GPIO_AF_1

void eeprom_read_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *rxptr, uint16_t nob);
void eeprom_transfer_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *txptr, uint16_t nob);
void eeprom_data_read_write(uint16_t mem_adr, uint8_t read_wr_bit, uint8_t *buff, uint16_t nob);

#endif
