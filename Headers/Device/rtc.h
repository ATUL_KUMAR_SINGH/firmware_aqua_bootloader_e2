

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_conf.h"


#ifndef __RTC_H_
#define __RTC_H_


struct RTC_Set_Time_struct
{
	uint8_t RTC_Hours;
	uint8_t RTC_Minutes;
	uint8_t RTC_Seconds;
	uint8_t RTC_H12;
	uint32_t RTC_Format;

};

struct RTC_Get_Time_struct
{
	uint8_t RTC_Hours;
	uint8_t RTC_Minutes;
	uint8_t RTC_Seconds;
	uint8_t RTC_H12;

};


void RTC_initialize(void);
void RTC_Config(void);


#endif



